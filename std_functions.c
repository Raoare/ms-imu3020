/**
 * @file std_functions.c
 * @author Richard Oare
 * @date 15 Jan 2019
 * This file provides basic funcitonality to be used in other files.
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * ms-imu3020 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "std_functions.h"

/** Variable to for testing endianness. */
const int test_endianness = 1;

/** Compares two strings to check if they are a match ignoring case
 @param[in] s1 First string
 @param[in] s2 Second string
 @return 0 if they match, -1 if one or both strings are NULL, or the
 difference betewen s1 and s2
 */
int my_strcmp(char *s1, char *s2) {
    unsigned int i;
    
    if (s1 == NULL || s2 == NULL) return -1;
    for (i = 0; s1[i] != '\0' && s2[i] != '\0'; i++) {
        if (toupper(s1[i]) != toupper(s2[i])) return s1[i] - s2[i];
    }
    if (s1[i] == s2[i]) return 0;
    else return -2; //Shouldn't be able to hit this case
}

/** Decodes user input from text to cmd_val and parses arguments
 @param[in] line User input string
 @param[out] args Parsed Arguments
 @param[in] maxNumArgs Maximum number of allowed arguments
 @return cmd_val (non-zero value) or 0 if invalid
 */
enum cmd_val decode_cmd(char *line, char **args, int maxNumArgs) {
    char *cmd = strtok(line, WHITESPACE);
    enum cmd_val cmdv;
    int i;
    
    if (0 == my_strcmp(cmd, CMD_SET_BAUD_RATE)) cmdv = OPT_SET_BAUD_RATE;
    else if (0 == my_strcmp(cmd, CMD_HELP)) cmdv = OPT_HELP;
    else if (0 == my_strcmp(cmd, CMD_RESET)) cmdv = OPT_RESET;
    else if (0 == my_strcmp(cmd, CMD_PING)) cmdv = OPT_PING;
    else if (0 == my_strcmp(cmd, CMD_START)) cmdv = OPT_START;
    else if (0 == my_strcmp(cmd, CMD_STOP)) cmdv = OPT_STOP;
    else if (0 == my_strcmp(cmd, CMD_EXIT)) cmdv = OPT_EXIT;
    else if (0 == my_strcmp(cmd, CMD_QUIT)) cmdv = OPT_EXIT;
    else if (0 == my_strcmp(cmd, CMD_SET_GPS_TIME)) cmdv = OPT_SET_GPS_TIME;
    else if (0 == my_strcmp(cmd, CMD_PARAM)) cmdv = OPT_GET_PARAMETERS;
    else if (0 == my_strcmp(cmd, CMD_GET_DEVICE_MESSAGES)) cmdv = OPT_GET_DEVICE_MESSAGES;
    else if (0 == my_strcmp(cmd, CMD_SET_FILTER)) cmdv = OPT_SET_FILTER;
    else if (0 == my_strcmp(cmd, CMD_SET_SAMPLE_RATE)) cmdv = OPT_SET_SAMPLE_RATE;
    else if (0 == my_strcmp(cmd, CMD_SELECT_SENSORS)) cmdv = OPT_SELECT_SENSORS;
    else if (0 == my_strcmp(cmd, CMD_SET_ACCEL_RANGE)) cmdv = OPT_SET_ACCEL_RANGE;
    else if (0 == my_strcmp(cmd, CMD_SET_GYRO_RANGE)) cmdv = OPT_SET_GYRO_RANGE;
    else if (0 == my_strcmp(cmd, CMD_SET_ALL)) cmdv = OPT_SET_ALL;
    else if (0 == my_strcmp(cmd, CMD_SET_TIME)) cmdv = OPT_SET_TIME;
    else return 0;
    
    args[0] = NULL;
    for (i = 0; i < maxNumArgs && NULL != (cmd = strtok(NULL, WHITESPACE)); i++) args[i] = cmd;
    
    if (i < maxNumArgs && args[i] != NULL) args[i] = NULL;
        
    return cmdv;
}

/** Ensures correct range of arguments are input
 @param[in] args User input arguments
 @param[in] minNumArgs Minimum number of allowed arguments
 @param[in] maxNumArgs Maximum number of allowed arguments
 @param[in] v Whether or not to be verbose
 @return true or false on whether or not the number of arguments fall
 into the specified range
 */
bool correct_num_args(char **args, int minNumArgs, int maxNumArgs, bool v) {
    int i;
    
    //Case: No Arguements Allowed
    if (args[0] == NULL && maxNumArgs == 0) return true;
    else if (maxNumArgs == 0) {
        if (v) fprintf(stderr, "%s", "No Arguments Allowed\n");
        return false;
    }
    
    //Ensure Minimum Number of Arguements
    for (i = 0; i < minNumArgs; i++) {
        if (args[i] == NULL) {
            if (v) fprintf(stderr, "%s", "Not Enough Arguments\n");
            return false;
        }
    }
    
    //Ensure Maximum Number of Arguements
    while (args[i] != NULL && i < maxNumArgs) i++;
    if (args[i] != NULL) {
        if (v) fprintf(stderr, "%s", "Too Many Args\n");
        return false;
    }
    
    return true;
}

/** Converts byte array to an abritrary value
 @param[in,out] a Byte array to convert.  May be flipped depending
 on endianness
 @param[in] size Bytes to convert
 @return Bytes to arbitrary value
 */
void* array_to_val(uint8_t *a, int size) {
    int i;
    uint8_t temp;
    
    if (!is_bigendian()) {
        for(i = 0; i < size/2; i++) {
            temp = a[i];
            a[i] = a[size-1-i];
            a[size-1-i] = temp;
        }
    }
    
    return a;
}
