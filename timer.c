/**
 * @file timer.c
 * @author Richard Oare
 * @date 15 Jan 2019
 * This file provides a timeout function using the SIGALRM signal.
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * ms-imu3020 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include "timer.h"

static struct itimerval tval;

/** Creates an alarm timeout using SIGALRM.  Calls the handler
 function upon completion.
 @param[in] handler Function to call upon timer completion
 @param[in] seconds Number of seconds to wait before SIGALRM
 is set
 @return None
 */
void interrupt_timer(void (*handler) (void), uint32_t seconds) {
    (void) signal(SIGALRM, (void*) handler);
    alarm(seconds);
}
