/**
 * @file stdin_control.h
 * @author Richard Oare
 * @date 15 Jan 2019
 * This file provides definitions for stdin_control.c
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * ms-imu3020 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef stdin_control_h
#define stdin_control_h

#include <stddef.h>
#include <stdio.h>
#include "imu_lib.h"
#include "std_functions.h"

/** @cond INCLUDE_WITH_DOXYGEN */
extern uint32_t stdin_arg[MAX_NUM_ARGS];

enum cmd_val fetch_decode(bool v);
/** @endcond */

#endif
