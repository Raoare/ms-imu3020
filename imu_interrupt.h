/**
 * @file imu_interrupt.h
 * @author Richard Oare
 * @date 15 Jan 2019
 * This file provides definitions for imu_interrupt.c
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * ms-imu3020 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <sys/time.h>
#include <wiringPi.h>
#include <stdlib.h>

// Wiring Pi GPIO Definitions
/** Time of Valid.  Defined in reference manual. Sets this to Physical
 pin 22 and BCM pin 25 */
#define TOV 6 //Physical pin 22 and BCM 25
/** Pulse Per Second.  Defined in reference manual. Sets this to Physical
 pin 16 and BCM pin 23 */
#define PPS 4 //Physical pin 16 and BCM 23
/** XTRIG used to sync.  Defined in reference manual. Sets this to Physical
 pin 18 and BCM pin 24 */
#define XTRIG 5 //Physical pin 18 and BCM 24

/** @cond INCLUDE_WITH_DOXYGEN */
void imu_interrupt_init(void (*imu_interrupt_handler)(void));
void stop_interrupt();
void restart_interrupt();
/** @endcond */
