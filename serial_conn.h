/**
 * @file serial_conn.h
 * @author Richard Oare
 * @date 31 Jan 2019
 * This file provides definitions for serial_conn.c
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * ms-imu3020 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef serial_conn_h
#define serial_conn_h

#include <stdio.h>
#include <termios.h>
#include <fcntl.h>
#include <sys/select.h>
#include <unistd.h>
#include <string.h>

#include "file_lock.h"

/** Maximum number of bytes to read from IMU. */
#define MAX_RECEIVED_PACKET_SIZE 261

/** @cond INCLUDE_WITH_DOXYGEN */
extern char recvBuf[MAX_RECEIVED_PACKET_SIZE];

void set_std_verbose();
int set_interface_attribs (int fd, int speed);
int init(char *file, int br);
void close_fd();
int txrx (char *msg, int msg_len);
int read_packet();
/** @endcond */

#endif
