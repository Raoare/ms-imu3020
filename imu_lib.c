/**
 * @file imu_lib.c
 * @author Richard Oare
 * @date 15 Jan 2019
 * This file takes the description of the Memsense IMU-3020 and defines
 * the functionality.
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * ms-imu3020 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
//TODO: Print if function code is request_cur
//TODO: Comment functions

#include "imu_lib.h"

/** Baud Rate to Use. */
static unsigned int baud_rate = 921600;
/** Number of Samples Taken */
static uint32_t sample_count = 0;
/** Flag to Use Sample Count */
static bool useSampleCount = false;
/** Device Absolute Path Variable */
static char deviceLocation[DEVICE_LOC_SIZE] = "/dev/serial0";
/** Transmit Buffer For IMU */
static char sendBuf[MAX_SEND_PACKET_SIZE];
/** Flag to Make Verbose Output */
static bool verbose = false;
/** Number of Leap Seconds Between UTC and GPS Time */
static int leapSeconds = 18;

/** Sets global variable for leap seconds
 @param[in] l Number to set leapSeconds variable to
 @return None
 */
void set_leap_seconds(int l) {
    leapSeconds = l;
}

/** Sets the IMU time based on system time
 @return ACK if successful, Error message otherwise
 */
//TODO: Simplify
enum return_values unix_time_to_gps_time() {
    uint32_t s;
    uint16_t w;
    time_t t;
    struct tm gpsEpoch = {0, 0, 0, 6, 0, 80}, *curTime, tunix = {0,0,0,0,0,0};
    char buf[26];
    
    //GPS Epoch
    
    gpsEpoch.tm_isdst = -1;        // Is DST on? 1 = yes, 0 = no, -1 = unknown
    
    t = time(NULL) - mktime(&gpsEpoch);
    printf("TIME: %ul\n",t);
    
    w = t / SECONDS_IN_WEEK;
    s = t % SECONDS_IN_WEEK;
    
    t = t + mktime(&gpsEpoch);
    curTime = localtime(&t);
    
    if (verbose) {
        printf("WEEK: %d\n",w);
        printf("SECONDS: %d\n",s);
        printf("LEAP SECONDS: %d\n",leapSeconds);
        strftime(buf, 26, "%Y:%m:%d %H:%M:%S", curTime);
        printf("%s\n",buf);
    }
    
    return set_gps_time(w,s);
}

/** Prints the GPS time from the IMU
 @param[in] s GPS seconds + microseconds
 @param[in] w GPS week number
 @param[in] f Flags from the IMU
 @return None
 */
void print_gps_time(double s, uint16_t w, uint16_t f) {
    struct tm gpsEpoch = {0, 0, 0, 6, 0, 80};
    char buf[30];
    double gpss = (uint64_t) (w * SECONDS_IN_WEEK) + s;
    time_t t = floor(gpss) + mktime(&gpsEpoch); //UNIX time
    
    strftime(buf, 30, "%m/%d/%Y %H:%M:%S", localtime(&t));
    printf("%s:%lf\n",buf,(pow(10,6) * (gpss-floor(gpss))));
}

/** Sets boolean value to tell functions to print
 @return None
 */
void make_imu_verbose() {
    verbose = true;
}

/** Sets initial baud rate to attempt
 @param[in] br Baud Rate to use
 @return None
 */
void set_baud_rate(unsigned int br) {
    baud_rate = br;
}

/** Sets the absolute path to the device
 @param[in] loc Absolute path to use
 @return None
 */
void set_device_location(char *loc) {
    strcpy(deviceLocation,loc);
}

/** Sets a number of samples to be taken.  Expected to quit the program
 after those samples are collected.
 @param[in] s Number of samples to collect
 @return None
 */
void set_num_samples(uint32_t s) {
    sample_count = s;
    useSampleCount = true;
}

/** Returns the UART baud rate being used
 @param[in] v Whether or not to be verbose
 @return UART Baud Rate
 */
unsigned int get_baud_rate(bool v) {
    if (v) printf("Baud Rate: %d\n", baud_rate);
    return baud_rate;
}

/** Opens a serial connection with the IMU starting with a predefined baud
 rate.  Connection is tested with a ping command.  All possible baud rates
 are tested until exhausted or successful.
 @return ACK if successful
 */
// TODO: check ping return and if an error is returned check it and try again if appropriate
enum return_values open_connection() {
    int attempts = 0, br = 0;
    int speeds[NUM_BAUD_RATE_OPTIONS] = {B9600,B19200,B115200,B230400,B460800,B921600};
    enum uart_rates urates[NUM_BAUD_RATE_OPTIONS] = {9600,19200,115200,230400,460800,921600};
    
    for (br = 0; br < NUM_BAUD_RATE_OPTIONS && baud_rate != urates[br]; br++);
    
    do {
        baud_rate = urates[br];
        if (verbose) printf("Trying Baud Rate: %d\n",baud_rate);
        init(deviceLocation,speeds[br]);
        if (ping() != ACK) (++br >= NUM_BAUD_RATE_OPTIONS) ? br = 0 : (void)0;
        else {
            if (verbose) printf("Successfully Connected\n");
            return ACK;
        }
    } while (++attempts < NUM_BAUD_RATE_OPTIONS);
    
    return ERR_NAK;
}

/** Computes the fletcher checksum
 @param[in] msg Message to compute
 @param[in] len Length of message to compute
 @return Computed checksum
 */
uint16_t fletcher(uint8_t* msg, uint8_t len) {
	unsigned int f1 = 0, f2 = 0, i;
	
    for (i = 0; i < len; i++) f2 += (f1 += msg[i]);
    
	return ((f1 % 256) << 8) + (f2 % 256);
}

/** Constructs a message to send to the IMU
 @param[in] msgh Message header struct to use
 @param[in] msg Message to be sent
 @param[in] msgl Length of message to send (doesn't include msgh length)
 @return Message size
 */
int construct_msg(struct message_header msgh, char *msg, unsigned int msgl) {
    int message_size = 0;
    uint16_t checksum;
    
    memcpy(sendBuf, (char*) &msgh, (message_size += sizeof(struct message_header)));
    if (msgl > 0) {
        memcpy(&sendBuf[message_size],msg,msgl);
        message_size += msgl;
    }
    checksum = fletcher(sendBuf,message_size);
    sendBuf[message_size++] = (checksum >> 8);
    sendBuf[message_size++] = checksum;
    return message_size;
}

/** Validates a message recieved from the IMU.  Doesn't work with data packets (0xA2)
 @param[in] rcv Received bytes buffer
 @param[in] br Bytes read
 @param[in] mh Message header to compare with
 @return Message size
 */
enum return_values validate_message(char *rcv, int br, struct message_header mh) {
    enum return_values rtn_val = ACK;
    uint16_t checksum;
    
    //Packet size appropriate
    if (br < MIN_RECEIVED_PACKET_SIZE || br > MAX_RECEIVED_PACKET_SIZE) {
        if (verbose) fprintf(stderr, "%s %d\n","Validation Failed [Packet Size]:",br);
        return ERR_INVALID_MSG;
    }
    if (((uint16_t) (rcv[0] << 8) + rcv[1]) != SYNC_BYTES) {
        if (verbose) fprintf(stderr, "%s %04x != %04x\n", "Bad Sync Bytes:",((uint16_t) (rcv[0] << 8) + rcv[1]),SYNC_BYTES);
        return ERR_SYNC_BYTES;
    }
    // Checksum
    else if (((rcv[br-2] << 8)+rcv[br-1]) != (checksum = fletcher(rcv,br-2))) {
        if (verbose) fprintf(stderr, "%s %04x != %04x\n", "Bad Checksum:",((rcv[br-2] << 8)+rcv[br-1]), checksum);
        return ERR_CHECKSUM_MISMATCH;
    }
    // Message Type
    else if (rcv[2] != mh.type) {
        if (verbose) fprintf(stderr, "%s %02x != %02x\n","Byte 2 (Message Type) Mismatch:",rcv[2],mh.type);
        return ERR_MSG_TYPE;
    }
    //Message ACK
    else if (rcv[4] != MSG_ACK) {
        if (verbose) fprintf(stderr, "%s %02x != %02x\n","Byte 4 (Message ACK) Mismatch:",rcv[4],MSG_ACK);
        return ERR_INVALID_MSG;
    }
    //Message Code
    else if (rcv[6] != mh.code) {
        if (verbose) fprintf(stderr, "%s %02x != %02x\n","Byte 6 (Message Code) Mismatch:",rcv[6],mh.code);
        return ERR_MISC;
    }
    //Message ACK
    else if (rcv[7] != ACK) {
        if (verbose) fprintf(stderr, "%s %02x != %02x\n","Byte 7 (ACK) Mismatch:",rcv[7],ACK);
        return ERR_NAK;
    }
                        
    return ACK;
}

/** Sends a ping to the IMU and receives the reply
 @return ACK if successful
 */
//TODO: print when received if verbose
enum return_values ping() {
    int msgLen,recvB;
    struct message_header msgh = {SYNC_BYTES, BASIC_CMD, 2, PING, 0};
    
    msgLen = construct_msg(msgh,0,0);
    recvB = txrx(sendBuf,msgLen);
    
    if (verbose) printf("Ping Sent\n");

    return validate_message(recvBuf,recvB,msgh);
}

/** Sends a request for device messages to the IMU and prints the reply
 in user readable form.
 @return ACK if successful
 */
enum return_values get_device_messages() {
    struct message_header msgh = {SYNC_BYTES, BASIC_CMD, 2, GET_MESSAGES, 0};
    int msgLen, recvB, i;
    enum avaiable_messages msg_type;
    enum return_values val;
    
    msgLen = construct_msg(msgh,0,0);
    recvB = txrx(sendBuf,msgLen);
    
    if (ACK != (val = validate_message(recvBuf,recvB,msgh))) return val;
    for (i = sizeof(struct message_header) + 4; i < recvB - 2; i++) {
        if(!(i % 2)) msg_type = (recvBuf[i] << 8);
        else {
            msg_type += recvBuf[i];
            switch(msg_type) {
                case SET_UART_BAUD_RATE_MSG:
                    printf("Set UART Baud Rate\n");
                    break;
                case PING_MSG:
                    printf("Ping\n");
                    break;
                case GET_MESSAGES_MSG:
                    printf("Get Device Messages (This Command)\n");
                    break;
                case SET_FILTER_MSG:
                    printf("Set Filter Rate\n");
                    break;
                case DEVICE_RST_MSG:
                    printf("Device Reset\n");
                    break;
                case SET_SAMPLE_RATE_MSG:
                    printf("Set IMU Sample Rate\n");
                    break;
                case GET_DEVICE_MODEL_MSG:
                    printf("Get Device Model\n");
                    break;
                case SELECT_SENSORS_MSG:
                    printf("Select Sensors\n");
                    break;
                case GET_DEVICE_SN_MSG:
                    printf("Get Device Serial Number\n");
                    break;
                case GET_IMU_SAMPLE_RATE_MSG:
                    printf("Get IMU Internal Sample Rate\n");
                    break;
                case GET_DEVICE_FW_MSG:
                    printf("Get Device Firmware\n");
                    break;
                case SET_ACCEL_RANGE_MSG:
                    printf("Set Acceleration Range\n");
                    break;
                case GET_DEVICE_CAL_MSG:
                    printf("Get Device Calibration Date\n");
                    break;
                case SET_GYRO_RANGE_MSG:
                    printf("Set Gyro Range\n");
                    break;
                case SET_GPS_TIME_MSG:
                    printf("Correlate GPS Time\n");
                    break;
                case SET_ALL_MSG:
                    printf("Configure All\n");
                    break;
                case DATA_ON_OFF_MSG:
                    printf("Set Data On/Off\n");
                    break;
                case XTRIG_ON_OFF_MSG:
                    printf("Set XTRIG On/OFF\n");
                    break;
                case SELECT_SENSORS_B_MSG:
                    printf("Select Sensors - Revision B\n");
                    break;
                case SET_AUX_ACCEL_RANGE_MSG:
                    printf("Configure Aux Acceleration Range\n");
                    break;
                case ACCEL_DATA_MSG:
                    printf("Data - Acceleration\n");
                    break;
                case ANGULAR_RATE_DATA_MSG:
                    printf("Data - Angular Rate\n");
                    break;
                case MAG_DATA_MSG:
                    printf("Data - Magnetic\n");
                    break;
                case DELTA_THETA_DATA_MSG:
                    printf("Data - Delta-Theta\n");
                    break;
                case DELTA_VELOCITY_DATA_MSG:
                    printf("Data - Velocity\n");
                    break;
                case PRESSURE_DATA_MSG:
                    printf("Data - Pressure\n");
                    break;
                case TEMP_DATA_MSG:
                    printf("Data - Temperature\n");
                    break;
                case GPS_TIME_DATA_MSG:
                    printf("Data - GPS Time\n");
                    break;
                case AUX_ACCEL_DATA_MSG:
                    printf("Data - Aux Acceleration\n");
                    break;
                default:
                    printf("Unknown Command: %02x",msg_type);
                    break;
            }
        }
    }
    return ACK;
}

/** Sends a reset command to the IMU.  Validation for the reset is unreliable
 so it is not used.
 @return ACK if successful
 */
//TODO: find way of ensuring reset worked
enum return_values reset() {
    struct message_header msgh = {SYNC_BYTES, BASIC_CMD, 2, DEVICE_RST, 0};
    int msgLen, recvB,i;
    enum return_values rv;
    
    msgLen = construct_msg(msgh,0,0);
    recvB = txrx(sendBuf,msgLen);
    
    // IMU does not reliably send the message stating that the device was reset, assume it was
    if (ACK == (rv = validate_message(recvBuf, recvB, msgh))) {
        printf("Device Reset\n");
    }
    //close_fd();
    //open_connection();
    //TODO: Ensure the device is still connected
    //if (ACK != ping()) open_connection();
    //else return rv;
    return rv;
}

/** Prints the IMU parameters in user readable form.
 @param[in] c Message code to send
 @param[in] beginStr String to print before printing data
 @return ACK if successful, Error code otherwise
 */
enum return_values get_device_param(enum message_codes c, char *beginStr) {
    struct message_header msgh = {SYNC_BYTES, BASIC_CMD, 2, c, 0};
    int msgLen, recvB, i;
    enum return_values rv;
    
    msgLen = construct_msg(msgh,0,0);
    recvB = txrx(sendBuf,msgLen);
    
    if (ACK == (rv = validate_message(recvBuf, recvB, msgh))) {
        recvBuf[recvB - 2] = (char) 0;
        for (i = sizeof(msgh) + 4; (i < (recvB - 2)) && isspace(recvBuf[i]); i++);
        printf("%s: %s\n",beginStr, &recvBuf[i]);
    }
    return rv;
}

/** Prints the IMU device model number in user readable form.
 @return ACK if successful, Error code otherwise
 */
enum return_values get_device_model() {
    return get_device_param(GET_DEVICE_MODEL,"Model Number");
}

/** Prints the IMU device serial number in user readable form.
 @return ACK if successful, Error code otherwise
 */
enum return_values get_device_sn() {
    return get_device_param(GET_DEVICE_SN,"Serial Number");
}

/** Prints the IMU device firmware version in user readable form.
 @return ACK if successful, Error code otherwise
 */
enum return_values get_device_fw() {
    return get_device_param(GET_DEVICE_FW,"Firmware Version");
}

/** Prints the IMU date of last calibration in user readable form.
 @return ACK if successful, Error code otherwise
 */
enum return_values get_device_cal() {
    return get_device_param(GET_DEVICE_CAL,"Date of Last Calibration");
}

/** Sets the GPS time on the IMU
 @param[in] gps_week GPS week number
 @param[in] time_seconds GPS seconds of week
 @return ACK if successful, Error code otherwise
 */
enum return_values set_gps_time(uint16_t gps_week, uint32_t time_seconds) {
    int msgLen, recvB;
    uint8_t msg[6] = {(gps_week >> 8), gps_week, (time_seconds >> 24), (time_seconds >> 16), (time_seconds >> 8), time_seconds};
    struct message_header msgh = {SYNC_BYTES, BASIC_CMD, 8, SET_GPS_TIME, 6};
    
    msgLen = construct_msg(msgh,msg,6);
    recvB = txrx(sendBuf,msgLen);
    
    return validate_message(recvBuf, recvB, msgh);
}

/** Sets the UART baud rate on the IMU
 @param[in] fn Function code to use
 @param[in] br UART baud rate to use
 @return ACK if successful, Error code otherwise
 */
//TODO: Reconnect to the device if the baud rate changed
enum return_values set_uart_baud_rate(enum function_codes fn, enum uart_rates br) {
    int msgLen, recvB;
    uint8_t msg[5] = {fn, (br >> 24), (br >> 16), (br >> 8), br};
    struct message_header msgh = {SYNC_BYTES, IMU_CMD, 7, SET_UART_BAUD_RATE, 5};
    
    msgLen = construct_msg(msgh,msg,5);
    recvB = txrx(sendBuf,msgLen);
    
    return validate_message(recvBuf, recvB, msgh);
}

/** Sets the IIR sampling filter on the IMU.
 @param[in] fn Function code to use
 @param[in] fs Filter setting to use
 @return ACK if successful, Error code otherwise
 */
enum return_values set_filter(enum function_codes fn, enum filter_settings fs) {
    int msgLen, recvB;
    uint8_t msg[2] = {fn, fs};
    struct message_header msgh = {SYNC_BYTES, IMU_CMD, 4, SET_FILTER, 2};
    
    msgLen = construct_msg(msgh,msg,2);
    recvB = txrx(sendBuf,msgLen);
    
    return validate_message(recvBuf, recvB, msgh);
}

/** Sets the sampling rate on the IMU.
 @param[in] fn Function code to use
 @param[in] sr Final sample rate is the internal sample rate divided
 by sr.
 @return ACK if successful, Error code otherwise
 */
enum return_values set_sample_rate(enum function_codes fn, uint16_t sr) {
    int msgLen, recvB;
    uint8_t msg[3] = {fn, (uint8_t) (sr >> 8), (uint8_t) sr};
    struct message_header msgh = {SYNC_BYTES, IMU_CMD, 5, SET_SAMPLE_RATE, 3};
    
    msgLen = construct_msg(msgh,msg,3);
    recvB = txrx(sendBuf,msgLen);
    
    return validate_message(recvBuf, recvB, msgh);
}

/** Selects which sensors to be returned from the IMU
 @param[in] fn Function code to use
 @param[in] s NULL deliminated list of sensors to use
 @return ACK if successful, Error code otherwise
 */
enum return_values select_sensors(enum function_codes fn, enum sensors *s) {
    int msgLen, recvB, i;
    uint8_t *msg;
    struct message_header msgh = {SYNC_BYTES, IMU_CMD, 0, SELECT_SENSORS, 0};
    
    //Count number of sensors found
    for (i = 0; i < 11 && s[i] != 0; i++);
    
    if (i < 1) return ERR_INVALID_MSG;
    else if (msg = malloc(2+i*sizeof(uint8_t))) {
        msg[0] = fn;
        msg[1] = 0;
        msgh.payload_size = (uint8_t) i+4;
        msgh.msg_len = (uint8_t) i+1;
        
        do {
            msg[i+1] = (uint8_t) s[i-1];
        } while(--i != 0);
    
        msgLen = construct_msg(msgh,msg,msgh.msg_len+1);
        recvB = txrx(sendBuf,msgLen);
    
        free(msg);
    
        return validate_message(recvBuf, recvB, msgh);
    }
    else return ERR_INVALID_MSG;
}

/** Requests the internal sample rate from the IMU
 @return ACK if successful, Error code otherwise
 */
//TODO: Have this return the sample rate
enum return_values get_internal_sample_rate() {
    int msgLen, recvB;
    enum return_values rv;
    struct message_header msgh = {SYNC_BYTES, IMU_CMD, 2, GET_IMU_SAMPLE_RATE, 0};
    
    msgLen = construct_msg(msgh,0,0);
    recvB = txrx(sendBuf,msgLen);
    
    if (0 == (rv = validate_message(recvBuf, recvB, msgh))) {
        printf("%s%d\n", "Internal Sample Rate: ", (uint16_t) (recvBuf[10] << 8) + (recvBuf[11]));
    }
    return rv;
}

/** Sets the accelerometer range for the IMU.  This is not supported
 per section 4.4.4 of the IMU Datasheet.
 @deprecated
 @param[in] fn Function code to use
 @param[in] ar Acceleration range to use
 @return ACK if successful, Error code otherwise
 */
enum return_values set_accel_range(enum function_codes fn, enum accel_range ar) {
    int msgLen, recvB;
    uint8_t msg[2] = {fn, ar};
    struct message_header msgh = {SYNC_BYTES, IMU_CMD, 4, SET_ACCEL_RANGE, 2};
    
    msgLen = construct_msg(msgh,msg,2);
    recvB = txrx(sendBuf,msgLen);
    
    return validate_message(recvBuf, recvB, msgh);
}

/** Sets the auxilary accelerometer range for the IMU.  This is not
 supported per section 4.4.4 of the IMU Datasheet.
 @deprecated
 @param[in] fn Function code to use
 @param[in] ar Acceleration range to use
 @return ACK if successful, Error code otherwise
 */
//TODO: Same problem as set_accel_range
enum return_values set_aux_accel_range(enum function_codes fn, enum accel_range ar) {
    int msgLen, recvB;
    uint8_t msg[2] = {fn, ar};
    struct message_header msgh = {SYNC_BYTES, IMU_CMD, 4, SET_AUX_ACCEL_RANGE, 2};
    
    msgLen = construct_msg(msgh,msg,2);
    recvB = txrx(sendBuf,msgLen);
    
    return validate_message(recvBuf, recvB, msgh);
}

/** Sets the gyroscope range for the IMU.
 @param[in] fn Function code to use
 @param[in] gr Gyroscope range to use
 @return ACK if successful, Error code otherwise
 */
enum return_values set_gyro_range(enum function_codes fn, enum gyro_range gr) {
    int msgLen, recvB;
    uint8_t msg[2] = {fn, gr};
    struct message_header msgh = {SYNC_BYTES, IMU_CMD, 4, SET_GYRO_RANGE, 2};
    
    msgLen = construct_msg(msgh,msg,2);
    recvB = txrx(sendBuf,msgLen);
    
    return validate_message(recvBuf, recvB, msgh);
}

/** Sets all parameters on the IMU.
 @param[in] fn Function code to use (Some function codes are invalid here).
 Only SAVE_CUR, LOAD_STARTUP, and SET_DEFAULT can be used.
 @return ACK if successful, Error code otherwise
 */
enum return_values set_all(enum function_codes fn) {
    int msgLen, recvB;
    uint8_t msg[1] = {fn};
    struct message_header msgh = {SYNC_BYTES, IMU_CMD, 3, SET_ALL, 1};
    
    if (fn != SAVE_CUR && fn != LOAD_STARTUP && fn != SET_DEFAULT) return ERR_INVALID_MSG;
    
    msgLen = construct_msg(msgh,msg,1);
    recvB = txrx(sendBuf,msgLen);
    
    return validate_message(recvBuf, recvB, msgh);
}

/** Sets the data sampling on or off on the IMU.
 @param[in] fn Function code to use
 @param[in] of TURN_ON or TURN_OFF
 @return ACK if successful, Error code otherwise
 */
enum return_values set_data_on_off(enum function_codes fn, enum turn_on_off of) {
    int msgLen, recvB;
    uint8_t msg[2] = {fn, of};
    struct message_header msgh = {SYNC_BYTES, IMU_CMD, 4, DATA_ON_OFF, 2};
    
    msgLen = construct_msg(msgh,msg,2);
    recvB = txrx(sendBuf,msgLen);
    
    return validate_message(recvBuf, recvB, msgh);
}

/** Sets the XTRIG on or off on the IMU.
 @param[in] fn Function code to use
 @param[in] of TURN_ON or TURN_OFF
 @return ACK if successful, Error code otherwise
 */
//TODO: test this
enum return_values set_xtrig_on_off(enum function_codes fn, enum turn_on_off of) {
    int msgLen, recvB;
    uint8_t msg[2] = {fn, of};
    struct message_header msgh = {SYNC_BYTES, IMU_CMD, 4, XTRIG_ON_OFF, 2};
    
    msgLen = construct_msg(msgh,msg,2);
    recvB = txrx(sendBuf,msgLen);
    
    return validate_message(recvBuf, recvB, msgh);
}

/** Selects which sensors to be returned from the IMU.  Use this if the firmware
 is above rev. B.
 @param[in] fn Function code to use
 @param[in] s NULL deliminated list of sensors to use
 @return ACK if successful, Error code otherwise
 */
enum return_values select_sensors_b(enum function_codes fn, enum sensors *s) {
    int msgLen, recvB, i;
    uint8_t *msg;
    struct message_header msgh = {SYNC_BYTES, IMU_CMD, 0, SELECT_SENSORS_B, 0};
    
    //Count number of sensors found and allocate memory
    for (i = 0; i < 11 && s[i] != 0; i++);
    
    if (i < 1) return ERR_INVALID_MSG;
    else if (msg = malloc(1+i*sizeof(uint8_t))) {
        msg[0] = fn;
        msgh.payload_size = (uint8_t) i+3;
        msgh.msg_len = (uint8_t) i+1;
        
        do {
            msg[i] = (uint8_t) s[i-1];
        } while(--i != 0);
        
        msgLen = construct_msg(msgh,msg,msgh.msg_len);
        recvB = txrx(sendBuf,msgLen);
        
        free(msg);
        
        return validate_message(recvBuf, recvB, msgh);
    }
    else return ERR_INVALID_MSG;
}

/** Fetches data packets from the IMU.  This fetches the packets, interperates them,
 and prints them accordingly.  Intended to be used in an interrupt.
 @return None
 */
void fetch_data() {
    int r = read_packet(), i = 5, j;
    enum sensors sensor_type;
    float val;
    uint16_t checksum, w, f;
    double s;
    
    if (r > MIN_RECEIVED_PACKET_SIZE) {
        //Validate packet
        if (((uint16_t)((recvBuf[0] << 8) + recvBuf[1]) != (uint16_t) SYNC_BYTES) || (recvBuf[2] != 0xa2)) {
            if (verbose) fprintf(stderr, "%s\n", "Packet Mismatch");
            return; //Data packet type validation
        }
        else if (((recvBuf[r-2] << 8)+recvBuf[r-1]) != (checksum = fletcher(recvBuf,r-2))) {
            if (verbose) printf("Bad Checksum: %04x != %04x\n",((recvBuf[r-2] << 8)+recvBuf[r-1]), checksum);
            return;  //Checksum validation
        }
        do {
            switch (sensor_type = recvBuf[i-1]) {
                case SENSOR_ACCEL:
                    if (verbose) printf("Acceleration: ");
                    for(j = 0; j < recvBuf[i]; j += 4) {
                        val = (*(float*) array_to_val(&(recvBuf[j+i+1]),sizeof(float)));
                        printf("%f ",val);
                    }
                    printf("\n");
                    break;
                case SENSOR_ANGULAR_RATE:
                    if (verbose) printf("Angular Rate: ");
                    for(j = 0; j < recvBuf[i]; j += 4) {
                        val = (*(float*) array_to_val(&(recvBuf[j+i+1]),sizeof(float)));
                        printf("%f ",val);
                    }
                    printf("\n");
                    break;
                case SENSOR_MAGNETIC:
                    if (verbose) printf("Magnetic: ");
                    for(j = 0; j < recvBuf[i]; j += 4) {
                        val = (*(float*) array_to_val(&(recvBuf[j+i+1]),sizeof(float)));
                        printf("%f ",val);
                    }
                    printf("\n");
                    break;
                case SENSOR_DELTA_THETA:
                    if (verbose) printf("Delta Theta: ");
                    for(j = 0; j < recvBuf[i]; j += 4) {
                        val = (*(float*) array_to_val(&(recvBuf[j+i+1]),sizeof(float)));
                        printf("%f ",val);
                    }
                    printf("\n");
                    break;
                case SENSOR_DELTA_VELOCITY:
                    if (verbose) printf("Delta Velocity: ");
                    for(j = 0; j < recvBuf[i]; j += 4) {
                        val = (*(float*) array_to_val(&(recvBuf[j+i+1]),sizeof(float)));
                        printf("%f ",val);
                    }
                    printf("\n");
                    break;
                case SENSOR_PRESSURE:
                    if (verbose) printf("Pressure: ");
                    val = (*(float*) array_to_val(&(recvBuf[i+1]),sizeof(float)));
                    printf("%f\n",val);
                    break;
                case SENSOR_TEMP:
                    if (verbose) printf("Temperature: ");
                    val = (*(float*) array_to_val(&(recvBuf[i+1]),sizeof(float)));
                    printf("%f\n",val);
                    break;
                case SENSOR_GPS_TIME:
                    if (verbose) printf("Time: ");

                    s = (*(double*) array_to_val(&(recvBuf[i+1]),sizeof(double)));
                    w = (*(uint16_t*) array_to_val(&(recvBuf[i+9]),sizeof(uint16_t)));
                    f = (*(uint16_t*) array_to_val(&(recvBuf[i+11]),sizeof(uint16_t)));
                    
                    print_gps_time(s,w,f);
                    break;
                case SENSOR_AUX_ACCEL:
                    if (verbose) printf("Acceleration: ");
                    for(j = 0; j < recvBuf[i]; j += 4) {
                        val = (*(float*) array_to_val(&(recvBuf[j+i+1]),sizeof(float)));
                        printf("%f ",val);
                    }
                    printf("\n");
                    break;
                default:
                    fprintf(stderr, "%s", "Invalid Sensor Type Read");
                    break;
            }
        } while ((r-2) > (i += (recvBuf[i] + 2)));
        if (useSampleCount) {
            if (--sample_count == 0) {
                fflush(stdout);
                close_fd();
                exit(EXIT_SUCCESS);
            }
        }
    }
}

/** Ensures the filter setting is valid.
 @param[in] f Filter setting to be used
 @param[in] v Whether or not to be verbose
 @return Valid filter setting, 0 otherwise
 */
enum filter_settings validate_filter_setting(uint32_t f, bool v) {
    switch (f) {
        case 10:
            return FILTER_10Hz;
            break;
        case 25:
            return FILTER_25Hz;
            break;
        case 50:
            return FILTER_50Hz;
            break;
        case 75:
            return FILTER_75Hz;
            break;
        case 100:
            return FILTER_100Hz;
            break;
        case 200:
            return FILTER_200Hz;
            break;
        default:
            if (v) fprintf(stderr, "%s", "Invalid Filter Setting\n");
            return 0;
            break;
    }
}

/** Ensures the gyroscope range setting is valid.
 @param[in] r Gyroscope range setting to use
 @param[in] v Whether or not to be verbose
 @return Valid gyroscope range setting, 0 otherwise
 */
enum gyro_range validate_gyro_range(uint32_t r, bool v) {
    switch (r) {
        case 120:
            return GYRO_RANGE_120;
            break;
        case 240:
            return GYRO_RANGE_240;
            break;
        case 480:
            return GYRO_RANGE_480;
            break;
        case 960:
            return GYRO_RANGE_960;
            break;
        case 1920:
            return GYRO_RANGE_1920;
            break;
        default:
            if (v) fprintf(stderr, "%s", "Invalid Gyro Range\n");
            return 0;
            break;
    }
}

/** Ensures the accelerometer range setting is valid.
 @param[in] r Accelerometer range setting to use
 @param[in] v Whether or not to be verbose
 @return Valid accelerometer range setting, 0 otherwise
 */
enum accel_range validate_accel_range(uint32_t r, bool v) {
    switch (r) {
        case 2:
            return ACCEL_RANGE_2;
            break;
        case 4:
            return ACCEL_RANGE_4;
            break;
        case 8:
            return ACCEL_RANGE_8;
            break;
        case 10:
            return ACCEL_RANGE_10;
            break;
        case 15:
            return ACCEL_RANGE_15;
            break;
        case 20:
            return ACCEL_RANGE_20;
            break;
        case 40:
            return ACCEL_RANGE_40;
            break;
        default:
            if (v) fprintf(stderr, "%s", "Invalid Accel Range\n");
            return 0;
            break;
    }
}

/** Ensures the UART baud rate setting is valid.
 @param[in] r UART baud rate setting to use
 @param[in] v Whether or not to be verbose
 @return Valid UART baud rate setting, 0 otherwise
 */
enum uart_rates validate_uart_rates(uint32_t r, bool v) {
    if ((r == UART_RATE_9600) || (r == UART_RATE_19200) || (r == UART_RATE_115200) || (r == UART_RATE_230400) || (r == UART_RATE_460800) || (r == UART_RATE_921600)) return ((enum uart_rates) r);
    else {
        if (v) fprintf(stderr, "%s", "Invalid UART Baud Rate\n");
        return 0;
    }
}

/** Converts a string to a function code.  Valid strings are predefined
 values USE_NEW, REQUEST_CUR, SAVE_CUR, LOAD_STARTUP, or SET_DEFAULT.
 @param[in] s String corresponding to function code
 @param[in] v Whether or not to be verbose
 @return Valid function code, 0 otherwise
 */
enum function_codes str_to_function_code(char *s, bool v) {
    enum function_codes x = 0;
    
    if (0 == my_strcmp(s,FN_CODE_USE_NEW)) x = USE_NEW;
    else if (0 == my_strcmp(s,FN_CODE_REQUEST_CUR)) x = REQUEST_CUR;
    else if (0 == my_strcmp(s,FN_CODE_SAVE)) x = SAVE_CUR;
    else if (0 == my_strcmp(s,FN_CODE_LOAD)) x = LOAD_STARTUP;
    else if (0 == my_strcmp(s,FN_CODE_SET_DEFAULT)) x = SET_DEFAULT;
                        
    if (v && 0 == x) fprintf(stderr, "%s", "Invalid Function Code\n");
    return x;
}

/** Converts a list of strings to a list of sensor codes.  Valid strings
 are predefined values SENSOR_STR_ACCEL, SENSOR_STR_ANGULAR_RATE, SENSOR_STR_MAG,
 SENSOR_STR_DELTA_THETA, SENSOR_STR_DELTA_VELOCITY, SENSOR_STR_PRESSURE,
 SENSOR_STR_TEMP, and SENSOR_STR_TIME.  This is also dependent on the device
 model number as some models have sensors others dont.
 @param[in] s String corresponding to sensor code
 @param[out] x Sensor list to write
 @param[in] max_len Maximum length of s or x, whichever is lower
 @param[in] v Whether or not to be verbose
 @return True if sensor list is valid, false otherwise.
 */
//TODO: Ensure valid with specific model number
bool str_to_sensors(char **s, enum sensors *x, int max_len, bool v) {
    int i, j;
    
    for (i = 0; s[i] != NULL && i < max_len; i++) {
        if (0 == my_strcmp(s[i],SENSOR_STR_ACCEL)) x[i] = SENSOR_ACCEL;
        else if (0 == my_strcmp(s[i],SENSOR_STR_ANGULAR_RATE)) x[i] = SENSOR_ANGULAR_RATE;
        else if (0 == my_strcmp(s[i],SENSOR_STR_MAG)) x[i] = SENSOR_MAGNETIC;
        else if (0 == my_strcmp(s[i],SENSOR_STR_DELTA_THETA)) x[i] = SENSOR_DELTA_THETA;
        else if (0 == my_strcmp(s[i],SENSOR_STR_DELTA_VELOCITY)) x[i] = SENSOR_DELTA_VELOCITY;
        else if (0 == my_strcmp(s[i],SENSOR_STR_PRESSURE)) x[i] = SENSOR_PRESSURE;
        else if (0 == my_strcmp(s[i],SENSOR_STR_TEMP)) x[i] = SENSOR_TEMP;
        else if (0 == my_strcmp(s[i],SENSOR_STR_TIME)) x[i] = SENSOR_GPS_TIME;
        else {
            if (v) fprintf(stderr, "%s%s%s", "Invalid Sensor Found: ", s[i], "\n");
            return false;
        }
        printf("SENSOR: %s\n",s[i]);
        
        //Check for duplicate
        for(j = 0; j < i; j++) {
            if (x[j] == x[i]) {
                if (v) fprintf(stderr, "%s%s%s", "Duplicate Sensor Found: ", s[i], "\n");
                return false;
            }
        }
    }
    if (i < max_len) x[i] = 0;
            
    return true;
}
