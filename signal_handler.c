/**
 * @file signal_handler.c
 * @author Richard Oare
 * @date 15 Jan 2019
 * This file provides functionality to exit the program safely upon
 * certain signals.  These signals are broken into error or termination
 * signals.
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * ms-imu3020 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "signal_handler.h"

/** Handles error signals
 @param[in] sig SIGNO received
 @return None
 */
void handle_error_int(int sig) {
    switch (sig) {
        case SIGFPE:
            fprintf(stderr, "%s", "SIGFPE: Math Error\n");
            break;
        case SIGILL:
            fprintf(stderr, "%s", "SIGILL: Illegal Instruction\n");
            break;
        case SIGSEGV:
            fprintf(stderr, "%s", "SIGSEGV: Read Write Outside Memory\n");
            break;
        case SIGBUS:
            fprintf(stderr, "%s", "SIGBUS: Bad Pointer\n");
            break;
        case SIGABRT:
            fprintf(stderr, "%s", "SIGABRT: Error Detected\n");
            break;
        case SIGTRAP:
            fprintf(stderr, "%s", "SIGTRAP: Trap, Breakpoint Set?\n");
            break;
        case SIGSYS:
            fprintf(stderr, "%s", "SIGSYS: Bad System Call\n");
            break;
        default:
            fprintf(stderr, "%s", "Unknown Error\n");
            break;
    }
    exit(EXIT_FAILURE);
}

/** Handles termination signals
 @param[in] sig SIGNO received
 @return None
 */
void handle_term_int(int sig) {
    switch (sig) {
        case SIGTERM:
            fprintf(stderr, "%s", "SIGTERM: Stop Program\n");
            break;
        case SIGINT:
            fprintf(stderr, "%s", "SIGINT: Program Interrupted\n");
            break;
        case SIGQUIT:
            fprintf(stderr, "%s", "SIGQUIT: Quit Program\n");
            break;
        case SIGKILL:
            fprintf(stderr, "%s", "SIGKILL: Kill Program\n");
            break;
        case SIGHUP:
            fprintf(stderr, "%s", "SIGHUP: Program Hangup\n");
            break;
        default:
            fprintf(stderr, "%s", "Unknown Termination\n");
            break;
    }
    exit(EXIT_SUCCESS);
}

/** Initializes error signals with handler
 @param[in] handler Function to call upon error signal
 @return None
 */
void init_error_sig(void (*handler) (void)) {
    signal(SIGFPE, (void*) handler);
    signal(SIGILL, (void*) handler);
    signal(SIGSEGV, (void*) handler);
    signal(SIGBUS, (void*) handler);
    signal(SIGABRT, (void*) handler);
    signal(SIGTRAP, (void*) handler);
    signal(SIGSYS, (void*) handler);
}

/** Initializes termination signals with handler
 @param[in] handler Function to call upon termination signal
 @return None
 */
void init_term_sig(void (*handler) (void)) {
    signal(SIGTERM, (void*) handler);
    //Control-c
    signal(SIGINT, (void*) handler);
    signal(SIGQUIT, (void*) handler);
    signal(SIGKILL, (void*) handler);
    signal(SIGHUP, (void*) handler);
}

/** Initializes error and termination signal handling
 @param[in] error_handler Function to call upon error signal
 @param[in] term_handler Function to call upon termination signal
 @return None
 */
void init_sig(void (*error_handler) (void), void (*term_handler) (void)) {
    init_error_sig(error_handler);
    init_term_sig(term_handler);
}
