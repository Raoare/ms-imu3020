/**
 * @file main.c
 * @author Richard Oare
 * @version 1.0
 * @date 15 Jan 2019
 * ms-imu3020 defines and functionalizes the Memsense IMU-3020 in
 * accordance with Memsense reference documentation.  This is dependent
 * on the wiringPi library.
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * ms-imu3020 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
//TODO: Make interrupt optional with timer interrupt as default
//TODO: Change pin interrupt to linux pin interrupt from wiringPi
//TODO: Add functionality for xtrig with input to pin selection

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include "timer.h"
#include "stdin_control.h"
#include "std_functions.h"
#include "signal_handler.h"
#include <time.h>

/** Allowed single character arguments */
#define ARGS "d:b:s:t:hc"

/** Stores option flags input through main function */
static long opt_flags;

/** Exit safely with Failure
 @param[in] err Error to print to stderr
 @return None
 */
void exit_error(char *err) {
    if (NULL != err) fprintf(stderr, "%s\n", err);
    close_fd();
    exit(EXIT_FAILURE);
}

/** Exit safely with success
 @return None
 */
void exit_prog() {
    close_fd();
    exit(EXIT_SUCCESS);
}

/** Resets the IMU and returns the return value
 @return ACK if successful, Error code otherwise
 */
enum return_values fn_reset() {
    return reset();
}

/** Exit safely with Failure
 @param[in] v True for verbose
 @param[in] attempts Maximum number of attempts to attempt execution.
 @return ACK if successful, Error code otherwise
 */
enum return_values fn_ping(bool v, int attempts) {
    enum return_values rtn;
    int i = 0;
    
    do {
        rtn = ping();
        if (v) (ACK == rtn) ? printf("Ping Successful\n") : printf("Ping Failed\n");
    } while(ACK != rtn && ++i < attempts);
    
    return rtn;
}

/** Sets the GPS time
 @param[in] w GPS week number
 @param[in] s GPS seconds of week number
 @param[in] v True for verbose
 @param[in] attempts Maximum number of attempts to attempt execution.
 @return ACK if successful, Error code otherwise
 */
enum return_values fn_set_gps_time(uint16_t w, uint32_t s, bool v, int attempts) {
    enum return_values rtn;
    int i = 0;
    
    do {
        rtn = set_gps_time(w,s);
        if (v) (ACK == rtn) ? printf("GPS Time Set\n") : printf("GPS Time Setting Failed\n");
    } while(rtn != ACK && ++i < attempts);
    
    return rtn;
}

/** Sets the time on the IMU to the time from the host.
 @param[in] v True for verbose
 @param[in] attempts Maximum number of attempts to attempt execution.
 @return ACK if successful, Error code otherwise
 */
enum return_values fn_set_time(bool v, int attempts) {
    enum return_values rtn;
    int i = 0;
    
    do {
        rtn = unix_time_to_gps_time();
        if (v) (ACK == rtn) ? printf("GPS Time Set\n") : printf("GPS Time Setting Failed\n");
    } while(rtn != ACK && ++i < attempts);
    
    return rtn;
}

/** Gets the parameters of the IMU and prints them.
 @param[in] v True for verbose
 @param[in] attempts Maximum number of attempts to attempt execution.
 @return ACK if successful, Error code otherwise
 */
enum return_values fn_get_params(bool v, int attempts) {
    enum return_values rtn, frtn = ACK;
    int i = 0;
    
    if (v) printf("\nDevice Parameters\n");
    
    get_baud_rate(true);
    
    do {
        if ((rtn = get_internal_sample_rate()) != ACK) frtn = rtn;
        if (v) (ACK == rtn) ? printf("Got Internal Sample Rate\n") : printf("Failed to Get Internal Sample Rate\n");
    } while (++i < attempts && rtn != ACK);
    
    i = 0;
    do {
        if ((rtn = get_device_model()) != ACK) frtn = rtn;
        if (v) (ACK == rtn) ? printf("Got Device Model Number\n") : printf("Failed to Get Device Model Number\n");
    } while (++i < attempts && rtn != ACK);
    
    i = 0;
    do {
        if ((rtn = get_device_sn()) != ACK) frtn = rtn;
        if (v) (ACK == rtn) ? printf("Got Device SN\n") : printf("Failed to Get Device SN\n");
    } while (++i < attempts && rtn != ACK);

    i = 0;
    do {
        if ((rtn = get_device_fw()) != ACK) frtn = rtn;
        if (v) (ACK == rtn) ? printf("Got Firmware Version\n") : printf("Failed to Get Firmware Version\n");
    } while (++i < attempts && rtn != ACK);
    
    i = 0;
    do {
        if ((rtn = get_device_cal()) != ACK) frtn = rtn;
        if (v) (ACK == rtn) ? printf("Got Device Cal Date\n") : printf("Failed to Get Device Cal Date\n");
    } while (++i < attempts && rtn != ACK);
    
    return frtn;
}

/** Sets the UART baud rate
 @param[in] f Function code
 @param[in] r UART baud rate
 @param[in] v True for verbose
 @param[in] attempts Maximum number of attempts to attempt execution.
 @return ACK if successful, Error code otherwise
 */
//TODO: reset baud rate, possibly connection
enum return_values fn_set_uart_baud(enum function_codes f, enum uart_rates r, bool v, int attempts) {
    enum return_values rtn;
    int i = 0;
    
    do {
        rtn = set_uart_baud_rate(f,r);
        if (v) (ACK == rtn) ? printf("UART Baud Rate Set\n") : printf("UART Baud Rate Set Failed\n");
    } while(rtn != ACK && ++i < attempts);
    
    return rtn;
}

/** Prints the list of IMU functions available.
 @param[in] v True for verbose
 @param[in] attempts Maximum number of attempts to attempt execution.
 @return ACK if successful, Error code otherwise
 */
enum return_values fn_get_messages(bool v, int attempts) {
    enum return_values rtn;
    int i = 0;
    
    if (v) printf("\nDevice Messages:\n");
        
    do {
        rtn = get_device_messages();
    } while(++i < attempts && rtn != ACK);
    
    return rtn;
}

/** Sets the IMU IIR filter.
 @param[in] f Function code
 @param[in] s Filter setting
 @param[in] v True for verbose
 @param[in] attempts Maximum number of attempts to attempt execution.
 @return ACK if successful, Error code otherwise
 */
enum return_values fn_set_filter(enum function_codes f, enum filter_settings s, bool v, int attempts) {
    enum return_values rtn;
    int i = 0;
        
    do {
        rtn = set_filter(f,s);
        if (v) (rtn == ACK) ? printf("Filter Set\n") : printf("Filter Setting Failed\n");
    } while(++i < attempts && rtn != ACK);
    
    return rtn;
}

/** Sets the IMU sample rate.
 @param[in] f Function code
 @param[in] s Divides the IMU internal sample rate by s to form final sample rate
 @param[in] v True for verbose
 @param[in] attempts Maximum number of attempts to attempt execution.
 @return ACK if successful, Error code otherwise
 */
enum return_values fn_set_sample_rate(enum function_codes f, uint16_t s, bool v, int attempts) {
    enum return_values rtn;
    int i = 0;
    
    do {
        rtn = set_sample_rate(f,s);
        if (v) (rtn == ACK) ? printf("Sample Rate Set\n") : printf("Sample Rate Setting Failed\n");
    } while(++i < attempts && rtn != ACK);

    return rtn;
}

/** Selects sensors to be returned from the IMU.
 @param[in] f Function code
 @param[in] s Array of sensors
 @param[in] v True for verbose
 @param[in] attempts Maximum number of attempts to attempt execution.
 @return ACK if successful, Error code otherwise
 */
enum return_values fn_select_sensors(enum function_codes f, enum sensors *s, bool v, int attempts) {
    enum return_values rtn;
    int i = 0;
    
    do {
        rtn = select_sensors_b(f,s);
        if (v) (rtn == ACK) ? printf("Sensor Select Set\n") : printf("Sensor Select Failed\n");
    } while(++i < attempts && rtn != ACK);
        
    return rtn;
}

/** Sets the acceleration range on the IMU.
 @deprecated
 @param[in] f Function code
 @param[in] r Acceleration range
 @param[in] v True for verbose
 @param[in] attempts Maximum number of attempts to attempt execution.
 @return ACK if successful, Error code otherwise
 */
enum return_values fn_set_accel(enum function_codes f, enum accel_range r, bool v, int attempts) {
    enum return_values rtn;
    int i = 0;
    
    do {
        rtn = set_accel_range(f,r);
        if (v) (rtn == ACK) ? printf("Accel Range Set\n") : printf("Accel Range Setting Failed\n");
    } while(++i < attempts && rtn != ACK);
        
    return rtn;
}

/** Sets the gyroscope range on the IMU
 @param[in] f Function code
 @param[in] r Gyroscope range
 @param[in] v True for verbose
 @param[in] attempts Maximum number of attempts to attempt execution.
 @return ACK if successful, Error code otherwise
 */
enum return_values fn_set_gyro(enum function_codes f, enum gyro_range r, bool v, int attempts) {
    enum return_values rtn;
    int i = 0;
    
    do {
        rtn = set_gyro_range(f,r);
        if (v) (rtn == ACK) ? printf("Gyro Range Set\n") : printf("Gyro Range Setting Failed\n");
    } while(++i < attempts && rtn != ACK);
        
    return rtn;
}

/** Sets all parameters on the IMU
 @param[in] f Function code
 @param[in] v True for verbose
 @param[in] attempts Maximum number of attempts to attempt execution.
 @return ACK if successful, Error code otherwise
 */
enum return_values fn_set_all(enum function_codes f, bool v, int attempts) {
    enum return_values rtn;
    int i = 0;
    
    do {
        rtn = set_all(f);
        if (v) (rtn == ACK) ? printf("Set All Success\n") : printf("Set All Failed\n");
    } while(++i < attempts && rtn != ACK);
        
    return rtn;
}

/** Starting point of the ms-3020 program.
 @param[in] argc Number of arguments
 @param[in] argv Arguments passed to program
 @return EXIT_SUCCESS if successful, EXIT_FAILURE otherwise
 */
int main(int argc, char** argv) {
    int c, temp_num;
    uint16_t gps_week_temp, sr_temp;
    uint32_t gps_time_seconds_temp, num_samples = 0, timeout = 0;
    enum function_codes uart_baud_fn_temp, set_fitler_fn_temp, set_sample_rate_fn_temp, select_sensors_fn_temp, set_accel_fn_temp, set_gyro_fn_temp, set_all_fn_temp, select_sensors_b_fn_temp;
    enum uart_rates br_temp;
    enum filter_settings fs_temp;
    enum sensors sensors_temp[MAX_NUM_ARGS], sensors_b_temp[MAX_NUM_ARGS];
    enum accel_range accel_range_temp;
    enum gyro_range gyro_range_temp;
    char *temp_str[MAX_NUM_ARGS];
    unsigned long numSamples = 0, stored_opt_flags = 0;
    enum cmd_val temp_cmd;
    bool verbose = false;
    
    //Catch signals to exit program
    init_sig(&exit_error,&exit_prog);
    
    while (1) {
        //Set the long options, set flags if needed
        static struct option long_options[] = {
            {CMD_VERBOSE, optional_argument, (int*) &opt_flags, OPT_VERBOSE},
            {CMD_RESET, no_argument, (int*) &opt_flags, OPT_RESET},
            {CMD_PING, no_argument, (int*) &opt_flags, OPT_PING},
            {CMD_SET_GPS_TIME, required_argument, (int*) &opt_flags, OPT_SET_GPS_TIME},
            {CMD_PARAM, no_argument, (int*) &opt_flags, OPT_GET_PARAMETERS},
            {CMD_GET_DEVICE_MESSAGES, no_argument, (int*) &opt_flags, OPT_GET_DEVICE_MESSAGES},
            {CMD_SET_BAUD_RATE, required_argument, (int*) &opt_flags, OPT_SET_BAUD_RATE},
            {CMD_SET_FILTER, required_argument, (int*) &opt_flags, OPT_SET_FILTER},
            {CMD_SET_SAMPLE_RATE, required_argument, (int*) &opt_flags, OPT_SET_SAMPLE_RATE},
            {CMD_SELECT_SENSORS, required_argument, (int*) &opt_flags, OPT_SELECT_SENSORS},
            {CMD_SET_ACCEL_RANGE, required_argument, (int*) &opt_flags, OPT_SET_ACCEL_RANGE},
            {CMD_SET_GYRO_RANGE, required_argument, (int*) &opt_flags, OPT_SET_GYRO_RANGE},
            {CMD_SET_ALL, required_argument, (int*) &opt_flags, OPT_SET_ALL},
            {CMD_SET_TIME, no_argument, (int*) &opt_flags, OPT_SET_TIME},
            {CMD_DEVICE, required_argument, 0, 'd'},
            {CMD_CMDLINE, no_argument, (int*) &opt_flags, OPT_CMD_LINE_CONTROL},
            {CMD_HELP, no_argument, 0, 'h'},
            {0, 0, 0, 0}
        };
        /* getopt_long stores the option index here. */
        int option_index = 0;
        
        c = getopt_long(argc, argv, ARGS, long_options, &option_index);
        
        /* Detect the end of the options. */
        if (c == -1) break;
        
        switch (c) {
            case 0:
                stored_opt_flags |= opt_flags;
                if (optarg) {
                    temp_num = 0;
                    temp_str[temp_num] = strtok(optarg, ":");
                    do {
                        temp_str[++temp_num] = strtok(NULL, ":");
                    } while(temp_str[temp_num] != NULL && temp_num < MAX_NUM_ARGS);
                }
                else temp_str[0] = NULL;
                
                switch (long_options[option_index].val) {
                    case OPT_VERBOSE:
                        verbose = true;
                        make_imu_verbose();
                        if (0 == my_strcmp(temp_str[0],"a")) set_std_verbose();
                        break;
                    case OPT_SET_GPS_TIME:
                        //Ensure Correct Number of Arguments
                        if (!correct_num_args(temp_str,2,2,true)) {
                            stored_opt_flags -= OPT_SET_GPS_TIME;
                            exit_error("Incorrect Number of Arguments In GPS Time Setting");
                        }
                        else {
                            gps_week_temp = (uint16_t) strtol(temp_str[0], NULL, 10);
                            gps_time_seconds_temp = (uint32_t) strtol(temp_str[1], NULL, 10);
                            //Ensure Valid GPS Week and Seconds
                            if (0 == gps_week_temp || 0 == gps_time_seconds_temp) {
                                stored_opt_flags -= OPT_SET_GPS_TIME;
                                if (0 == gps_week_temp) exit_error("GPS Week Time Invalid");
                                else exit_error("GPS Seconds Time Invalid");
                            }
                        }
                        break;
                    case OPT_SET_BAUD_RATE:
                        //Ensure Correct Number of Arguments
                        if (!correct_num_args(temp_str,2,2,true)) {
                            stored_opt_flags -= OPT_SET_BAUD_RATE;
                            exit_error("Incorrect Number of Arguments In Setting Baud Rate");
                        }
                        //Ensure Correct Function Code
                        else if (0 == (uart_baud_fn_temp = str_to_function_code(temp_str[0], true))) {
                            stored_opt_flags -= OPT_SET_BAUD_RATE;
                            exit_error("Incorrect Function Code In Setting Baud Rate");
                        }
                        //Ensure Valid UART Baud Rate
                        else if (0 == validate_uart_rates((uart_baud_fn_temp = strtol(temp_str[1], NULL, 10)), true)) {
                            stored_opt_flags -= OPT_SET_BAUD_RATE;
                            exit_error("Incorrect Baud Rate In Setting Baud Rate");
                        }
                        break;
                    case OPT_SET_FILTER:
                        //Ensure Correct Number of Arguments
                        if (!correct_num_args(temp_str,2,2,true)) {
                            stored_opt_flags -= OPT_SET_FILTER;
                            exit_error("Incorrect Number of Arguments In Setting Filter");
                        }
                        //Ensure Correct Function Code
                        else if (0 == (set_fitler_fn_temp = str_to_function_code(temp_str[0], true))) {
                            stored_opt_flags -= OPT_SET_FILTER;
                            exit_error("Incorrect Function Code In Setting Filter");
                        }
                        //Ensure Valid Filter Setting
                        else if (0 == validate_filter_setting((fs_temp = strtol(temp_str[1], NULL, 10)), true)) {
                            stored_opt_flags -= OPT_SET_FILTER;
                            exit_error("Incorrect Filter Setting In Setting Filter");
                        }
                        break;
                    case OPT_SET_SAMPLE_RATE:
                        //Ensure Correct Number of Arguments
                        if (!correct_num_args(temp_str,2,2,true)) {
                            stored_opt_flags -= OPT_SET_SAMPLE_RATE;
                            exit_error("Incorrect Number of Arguments In Setting Sample Rate");
                        }
                        //Ensure Correct Function Code
                        else if (0 == (set_sample_rate_fn_temp = str_to_function_code(temp_str[0], true))) {
                            stored_opt_flags -= OPT_SET_SAMPLE_RATE;
                            exit_error("Incorrect Function Code In Setting Sample Rate");
                        }
                        //Ensure Valid Sample Rate
                        else {
                            sr_temp = (uint16_t) strtol(temp_str[1], NULL, 10);
                            if (sr_temp > 800 || sr_temp < 1) {
                                exit_error("Incorrect Sample Rate In Setting Sample Rate");
                                stored_opt_flags -= OPT_SET_SAMPLE_RATE;
                            }
                        }
                        break;
                    case OPT_SELECT_SENSORS:
                        //Ensure Correct Number of Arguments
                        if (!correct_num_args(temp_str,2,10,true)) {
                            stored_opt_flags -= OPT_SELECT_SENSORS;
                            exit_error("Incorrect Number of Arguments In Selecting Sensors");
                        }
                        //Ensure Correct Function Code
                        else if (0 == (select_sensors_fn_temp = str_to_function_code(temp_str[0], true))) {
                            stored_opt_flags -= OPT_SELECT_SENSORS;
                            exit_error("Incorrect Function Code In Selecting Sensors");
                        }
                        //Ensure Valid Sensors
                        else if (!str_to_sensors(&(temp_str[1]), sensors_temp, MAX_NUM_ARGS, true)) stored_opt_flags -= OPT_SELECT_SENSORS;
                        break;
                    case OPT_SET_ACCEL_RANGE:
                        //Ensure Correct Number of Arguments
                        if (!correct_num_args(temp_str,2,2,true)) {
                            stored_opt_flags -= OPT_SET_ACCEL_RANGE;
                            exit_error("Incorrect Number of Arguments In Setting Accel Range");
                        }
                        //Ensure Correct Function Code
                        else if (0 == (set_accel_fn_temp = str_to_function_code(temp_str[0], true))) {
                            stored_opt_flags -= OPT_SET_ACCEL_RANGE;
                            exit_error("Incorrect Function Code In Setting Accel Range");
                        }
                        //Ensure Valid Acceleration Range
                        else if (0 == (accel_range_temp = validate_accel_range(strtol(temp_str[1], NULL, 10), true))) {
                            stored_opt_flags -= OPT_SET_ACCEL_RANGE;
                            exit_error("Incorrect Accelerometer Range In Setting Accel Range");
                        }
                        break;
                    case OPT_SET_GYRO_RANGE:
                        //Ensure Correct Number of Arguments
                        if (!correct_num_args(temp_str,2,2,true)) {
                            stored_opt_flags -= OPT_SET_GYRO_RANGE;
                            exit_error("Incorrect Number of Arguments In Setting Gyro Range");
                        }
                        //Ensure Correct Function Code
                        else if (0 == (set_gyro_fn_temp = str_to_function_code(temp_str[0], true))) {
                            stored_opt_flags -= OPT_SET_GYRO_RANGE;
                            exit_error("Incorrect Function Code In Setting Gyro Range");
                        }
                        //Ensure Correct Gyroscope Range
                        else if (0 == (gyro_range_temp = validate_gyro_range(strtol(temp_str[1], NULL, 10), true))) {
                            stored_opt_flags -= OPT_SET_GYRO_RANGE;
                            exit_error("Incorrect Gyroscope Range In Setting Gyro Range");
                        }
                        break;
                    case OPT_SET_ALL:
                        //TODO: This one can only use 0x03, 0x04, 0x05 function codes
                        //Ensure Correct Number of Arguments
                        if (!correct_num_args(temp_str,1,1,true)) {
                            stored_opt_flags -= OPT_SET_ALL;
                            exit_error("Incorrect Number of Arguments In Setting All");
                        }
                        //Ensure Correct Function Code
                        else if (0 == (set_all_fn_temp = str_to_function_code(temp_str[0], true))) {
                            stored_opt_flags -= OPT_SET_ALL;
                            exit_error("Incorrect Function Code In Setting All");
                        }
                        break;
                }
                break;
            case 'd':
                set_device_location(optarg);
                break;
            case 'b':
                if (0 != (temp_num = validate_uart_rates((uint32_t) strtol(optarg, NULL, 10), true))) set_baud_rate(temp_num);
                else exit_error("Incorrect Baud Rate Setting");
                break;
            case 's':
                if ((num_samples = strtol(optarg, NULL, 10)) == 0) exit_error("Incorrect Number of Samples");
                else set_num_samples(num_samples);
                break;
            case 't':
                if ((timeout = strtol(optarg, NULL, 10)) == 0) exit_error("Invalid or Missing Timeout");
                else interrupt_timer(&exit_prog, timeout);
                break;
            case 'c':
                stored_opt_flags |= OPT_CMD_LINE_CONTROL;
                break;
            case 'h':
                printf("%s", USAGE_HELP);
                break;
            case '?':
                /* getopt_long already printed an error message. */
                exit_error("");
                break;
            default:
                abort();
                break;
        }
    }
    
    if (ACK != open_connection()) exit_error("Cannot Open Device");
    //Make sure device isn't spitting out data
    set_data_on_off(USE_NEW, TURN_OFF);
    
    if (stored_opt_flags & OPT_SET_BAUD_RATE) fn_set_uart_baud(uart_baud_fn_temp, br_temp, verbose, 2);
    if (stored_opt_flags & OPT_RESET) fn_reset();
    if (stored_opt_flags & OPT_PING) fn_ping(verbose, 2);
    if (stored_opt_flags & OPT_GET_PARAMETERS) fn_get_params(verbose, 2);
    if (stored_opt_flags & OPT_GET_DEVICE_MESSAGES) fn_get_messages(verbose, 2);
    if (stored_opt_flags & OPT_SET_GPS_TIME) fn_set_gps_time(gps_week_temp, gps_time_seconds_temp, verbose, 2);
    if (stored_opt_flags & OPT_SET_TIME) fn_set_time(verbose, 2);
    if (stored_opt_flags & OPT_SET_FILTER) fn_set_filter(set_fitler_fn_temp, fs_temp, verbose, 2);
    if (stored_opt_flags & OPT_SET_SAMPLE_RATE) fn_set_sample_rate(set_sample_rate_fn_temp, sr_temp, (bool) (stored_opt_flags & OPT_VERBOSE), 2);
    if (stored_opt_flags & OPT_SELECT_SENSORS) fn_select_sensors(select_sensors_fn_temp, sensors_temp, verbose, 2);
    if (stored_opt_flags & OPT_SET_ACCEL_RANGE) fn_set_accel(set_accel_fn_temp, accel_range_temp, verbose, 2);
    if (stored_opt_flags & OPT_SET_GYRO_RANGE) fn_set_gyro(set_gyro_fn_temp, gyro_range_temp, verbose, 2);
    if (stored_opt_flags & OPT_SET_ALL) fn_set_all(set_all_fn_temp, verbose, 2);
    
    //Start collecting data
    imu_interrupt_init(&fetch_data);
    if (!(stored_opt_flags & OPT_CMD_LINE_CONTROL)) set_data_on_off(USE_NEW, TURN_ON);
    else stop_interrupt();
    
    while(1) {
        if (stored_opt_flags & OPT_CMD_LINE_CONTROL) {
            if (0 != (temp_cmd = fetch_decode(verbose))) {
                switch (temp_cmd) {
                    case OPT_SET_BAUD_RATE:
                        fn_set_uart_baud((enum function_codes) stdin_arg[0], (enum uart_rates) stdin_arg[1], verbose, 2);
                        break;
                    case OPT_RESET:
                        fn_reset();
                        break;
                    case OPT_PING:
                        fn_ping(verbose, 2);
                        break;
                    case OPT_START:
                        set_data_on_off(USE_NEW, TURN_ON);
                        restart_interrupt();
                        break;
                    case OPT_STOP:
                        set_data_on_off(USE_NEW, TURN_OFF);
                        stop_interrupt();
                        break;
                    case OPT_EXIT:
                        exit_prog();
                        break;
                    case OPT_SET_GPS_TIME:
                        fn_set_gps_time((uint16_t) stdin_arg[0], (uint32_t) stdin_arg[1], verbose, 2);
                        break;
                    case OPT_GET_PARAMETERS:
                        fn_get_params(verbose, 2);
                        break;
                    case OPT_GET_DEVICE_MESSAGES:
                        fn_get_messages(verbose, 2);
                        break;
                    case OPT_SET_FILTER:
                        fn_set_filter((enum function_codes) stdin_arg[0], (enum filter_settings) stdin_arg[1], true, 2);
                        break;
                    case OPT_SET_SAMPLE_RATE:
                        //Note that the rate ends up as Internal sample rate (800) / stdin_arg[1]
                        fn_set_sample_rate((enum function_codes) stdin_arg[0], (uint16_t) stdin_arg[1], true, 2);
                        break;
                    case OPT_SELECT_SENSORS:
                        fn_select_sensors((enum function_codes) stdin_arg[0], (enum sensors*) &(stdin_arg[1]), true, 2);
                        break;
                    case OPT_SET_ACCEL_RANGE:
                        //TODO: Section 4.4.4 of the IMU Datasheet eliminates this
                        fn_set_accel((enum function_codes) stdin_arg[0], (enum accel_range) stdin_arg[1], true, 2);
                        break;
                    case OPT_SET_GYRO_RANGE:
                        fn_set_gyro((enum function_codes) stdin_arg[0], (enum gyro_range) stdin_arg[1], true, 2);
                        break;
                    case OPT_SET_ALL:
                        fn_set_all((enum function_codes) stdin_arg[0], true, 2);
                        break;
                    case OPT_SET_TIME:
                        fn_set_time(verbose, 2);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
