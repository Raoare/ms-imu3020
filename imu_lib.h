/**
 * @file imu_lib.h
 * @author Richard Oare
 * @date 15 Jan 2019
 * This file provides definitions for imu_lib.c
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * ms-imu3020 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include "serial_conn.h"
#include "imu_interrupt.h"
#include <stdbool.h>
#include "std_functions.h"
#include <math.h>
#include <time.h>

/** IMU Sync Bytes */
#define SYNC_BYTES 0xa5a5
/** IMU Minimum Packet Size in Bytes */
#define MIN_RECEIVED_PACKET_SIZE 10
/** IMU Number of Baud Rate Options */
#define NUM_BAUD_RATE_OPTIONS 6
/** IMU Maximum Packet Size to Send */
#define MAX_SEND_PACKET_SIZE 14
/** IMU Maximum File Location Size */
#define DEVICE_LOC_SIZE 255
/** IMU Maximum Sample Rate */
#define MAX_SAMPLE_RATE 65535
/** IMU Minimum Sample Rate */
#define MIN_SAMPLE_RATE 1
/** IMU Accelerometer Sensor for User Input */
#define SENSOR_STR_ACCEL "A"
/** IMU Angular Rate Sensor for User Input */
#define SENSOR_STR_ANGULAR_RATE "AR"
/** IMU Magnetometer Sensor for User Input */
#define SENSOR_STR_MAG "M"
/** IMU Delta Theta Sensor for User Input */
#define SENSOR_STR_DELTA_THETA "DT"
/** IMU Delta Velocity Sensor for User Input */
#define SENSOR_STR_DELTA_VELOCITY "DV"
/** IMU Pressure Sensor for User Input */
#define SENSOR_STR_PRESSURE "P"
/** IMU Temperature Sensor for User Input */
#define SENSOR_STR_TEMP "TP"
/** IMU Time Sensor for User Input */
#define SENSOR_STR_TIME "T"

/** Number of Seconds in a Week */
#define SECONDS_IN_WEEK (60*60*24*7)

/** IMU Function Code to Save Current */
#define FN_CODE_SAVE "SAVE"
/** IMU Function Code to Load Default */
#define FN_CODE_LOAD "LOAD"
/** IMU Function Code to Set Default */
#define FN_CODE_SET_DEFAULT "SETD"
/** IMU Function Code to Use New */
#define FN_CODE_USE_NEW "USE"
/** IMU Function Code to Set Default */
#define FN_CODE_REQUEST_CUR "CUR"

/*! Message Types (Page 4) From MS-3020 Documentation.*/
enum message_types {
    BASIC_CMD = 1,  /*!< Message Type for Basic Command */
    IMU_CMD,        /*!< Message Type for IMU Command */
    IMU_DATA = 0xA2 /*!< Message Type for IMU Data */
};

/*! Available Message Codes From MS-3020 Documentation.*/
enum message_codes {
    SET_UART_BAUD_RATE = 1,     /*!< Message Code for Set UART Baud Rate */
    PING,                       /*!< Message Code for Ping Message */
    GET_MESSAGES,               /*!< Message Code for Get Available Messages */
    SET_FILTER = 3,             /*!< Message Code for Setting IIR Filter */
    DEVICE_RST,                 /*!< Message Code for Device Reset */
    SET_SAMPLE_RATE = 4,        /*!< Message Code for Setting Sample Rate */
    GET_DEVICE_MODEL,           /*!< Message Code for Getting the Device Model Number */
    SELECT_SENSORS = 5,         /*!< Message Code for Selecting Sensors */
    GET_DEVICE_SN,              /*!< Message Code for Getting the Device Serial Number */
    GET_IMU_SAMPLE_RATE = 6,    /*!< Message Code for Getting the IMU Sample Rate */
    GET_DEVICE_FW,              /*!< Message Code for Getting the Device Firmware Version */
    SET_ACCEL_RANGE = 7,        /*!< Message Code for Setting the Accelerometer Range */
    GET_DEVICE_CAL,             /*!< Message Code for Getting the IMU Calibration Date */
    SET_GYRO_RANGE = 8,         /*!< Message Code for Setting the Gyroscope Range */
    SET_GPS_TIME,               /*!< Message Code for Setting the GPS Time */
    SET_ALL = 9,                /*!< Message Code for Setting All Parameters */
    DATA_ON_OFF,                /*!< Message Code for Turning the Data Output On or Off */
    XTRIG_ON_OFF,               /*!< Message Code for Turning the XTRIG On or Off */
    SELECT_SENSORS_B,           /*!< Message Code for Selecting the Sensors On Rev. B Devices */
    SET_AUX_ACCEL_RANGE,        /*!< Message Code for Setting the Auxilary Accelerometer Range */
    MSG_ACK = 0x80              /*!< Message Code for Message Acknowledgement */
};

/*! Available Message Codes From MS-3020 Documentation.*/
enum avaiable_messages {
    SET_UART_BAUD_RATE_MSG = 0x0201,    /*!< Message Code for Set UART Baud Rate */
    PING_MSG = 0x0102,                  /*!< Message Code for Ping Message */
    GET_MESSAGES_MSG = 0x0103,          /*!< Message Code for Get Available Messages */
    SET_FILTER_MSG = 0x0203,            /*!< Message Code for Setting IIR Filter */
    DEVICE_RST_MSG = 0x0104,            /*!< Message Code for Device Reset */
    SET_SAMPLE_RATE_MSG = 0x0204,       /*!< Message Code for Setting Sample Rate */
    GET_DEVICE_MODEL_MSG = 0x0105,      /*!< Message Code for Getting the Device Model Number */
    SELECT_SENSORS_MSG = 0x0205,        /*!< Message Code for Selecting Sensors */
    GET_DEVICE_SN_MSG = 0x0106,         /*!< Message Code for Getting the Device Serial Number */
    GET_IMU_SAMPLE_RATE_MSG = 0x0206,   /*!< Message Code for Getting the IMU Sample Rate */
    GET_DEVICE_FW_MSG = 0x0107,         /*!< Message Code for Getting the Device Firmware Version */
    SET_ACCEL_RANGE_MSG = 0x0207,       /*!< Message Code for Setting the Accelerometer Range */
    GET_DEVICE_CAL_MSG = 0x0108,        /*!< Message Code for Getting the IMU Calibration Date */
    SET_GYRO_RANGE_MSG = 0x0208,        /*!< Message Code for Setting the Gyroscope Range */
    SET_GPS_TIME_MSG = 0x0109,          /*!< Message Code for Setting the GPS Time */
    SET_ALL_MSG = 0x0209,               /*!< Message Code for Setting All Parameters */
    DATA_ON_OFF_MSG = 0x020a,           /*!< Message Code for Turning the Data Output On or Off */
    XTRIG_ON_OFF_MSG = 0x020b,          /*!< Message Code for Turning the XTRIG On or Off */
    SELECT_SENSORS_B_MSG = 0x020c,      /*!< Message Code for Selecting the Sensors On Rev. B Devices */
    SET_AUX_ACCEL_RANGE_MSG = 0x020d,   /*!< Message Code for Setting the Auxilary Accelerometer Range */
    ACCEL_DATA_MSG = 0xa281,            /*!< Message Code for Accelerometer Data */
    ANGULAR_RATE_DATA_MSG = 0xa282,     /*!< Message Code for Angular Rate Data */
    MAG_DATA_MSG = 0xa283,              /*!< Message Code for Magnetometer Data */
    DELTA_THETA_DATA_MSG = 0xa284,      /*!< Message Code for Delta Theta Data */
    DELTA_VELOCITY_DATA_MSG = 0xa285,   /*!< Message Code for Delta Velocity Data */
    PRESSURE_DATA_MSG = 0xa286,         /*!< Message Code for Pressure Data */
    TEMP_DATA_MSG = 0xa287,             /*!< Message Code for Temperature Data */
    GPS_TIME_DATA_MSG = 0xa288,         /*!< Message Code for GPS Time Data */
    AUX_ACCEL_DATA_MSG = 0xa289         /*!< Message Code for Auxilary Accelerometer Data */
    //MSG_ACK = 0x80
};

/*! Table 18, 22, 26, 29, 35, 39, 43 (Limited), 46, 50, 54, and 39 Settings
 (Page 19, 21, 23, 25, 28, 30, 32, 33, 35, 37, and 39) from MS-3020 Documentation.*/
enum function_codes {
    USE_NEW = 1,    /*!< Function Code for Use New */
    REQUEST_CUR,    /*!< Function Code for Request Current */
    SAVE_CUR,       /*!< Function Code for Save Current */
    LOAD_STARTUP,   /*!< Function Code for Load Startup */
    SET_DEFAULT     /*!< Function Code for Set Default */
};

/*! Table 30 and 55 Settings (Page 25 and 37) from MS-3020 Documentation.*/
enum sensors {
    SENSOR_ACCEL = 0x81,    /*!< Sensor Code for Accelerometer (A) */
    SENSOR_ANGULAR_RATE,    /*!< Sensor Code for Angular Rate (AR) */
    SENSOR_MAGNETIC,        /*!< Sensor Code for Magnetometer (MAG) */
    SENSOR_DELTA_THETA,     /*!< Sensor Code for Delta Theta (DT) */
    SENSOR_DELTA_VELOCITY,  /*!< Sensor Code for Delta Velocity (DV) */
    SENSOR_PRESSURE,        /*!< Sensor Code for Pressure (P) */
    SENSOR_TEMP,            /*!< Sensor Code for Temperature (TP) */
    SENSOR_GPS_TIME,        /*!< Sensor Code for GPS Time (T) */
    SENSOR_AUX_ACCEL        /*!< Sensor Code for Auxilary Accelerometer (AA) */
    //TODO: [AUX_ACCEL] Table doesnt have this, but the end does...?
};

/*! Table 19 Settings (Page 19) from MS-3020 Documentation.*/
enum uart_rates {
    UART_RATE_9600 = 9600,      /*!< Set Baud Rate to 9600 BPS */
    UART_RATE_19200 = 19200,    /*!< Set Baud Rate to 19200 BPS */
    UART_RATE_115200 = 115200,  /*!< Set Baud Rate to 115200 BPS */
    UART_RATE_230400 = 230400,  /*!< Set Baud Rate to 230400 BPS */
    UART_RATE_460800 = 460800,  /*!< Set Baud Rate to 460800 BPS */
    UART_RATE_921600 = 921600   /*!< Set Baud Rate to 921600 BPS */
};

/*! Table 23 Settings (Page 21) from MS-3020 Documentation.*/
enum filter_settings {
    FILTER_DISABLE, /*!< Disable IIR Filter */
    FILTER_25Hz,    /*!< Set IIR Filter to 25 Hertz */
    FILTER_50Hz,    /*!< Set IIR Filter to 50 Hertz */
    FILTER_75Hz,    /*!< Set IIR Filter to 75 Hertz */
    FILTER_100Hz,   /*!< Set IIR Filter to 100 Hertz */
    FILTER_10Hz,    /*!< Set IIR Filter to 10 Hertz */
    FILTER_200Hz    /*!< Set IIR Filter to 200 Hertz */
};

/*! Table 36 and 59 Settings (Page 28 and 39) from MS-3020 Documentation.*/
enum accel_range {
    ACCEL_RANGE_2 = 1,  /*!< Accelerometer Range +/-2 G */
    ACCEL_RANGE_4,      /*!< Accelerometer Range +/-4 G */
    ACCEL_RANGE_8,      /*!< Accelerometer Range +/-8 G */
    ACCEL_RANGE_10,     /*!< Accelerometer Range +/-10 G */
    ACCEL_RANGE_15,     /*!< Accelerometer Range +/-15 G */
    ACCEL_RANGE_20,     /*!< Accelerometer Range +/-20 G */
    ACCEL_RANGE_40      /*!< Accelerometer Range +/-40 G */
};

/*! Table 40 Settings (Page 30) from MS-3020 Documentation.*/
enum gyro_range {
    GYRO_RANGE_120 = 1, /*!< Gyro Range +/-120 Degrees */
    GYRO_RANGE_240,     /*!< Gyro Range +/-240 Degrees */
    GYRO_RANGE_480,     /*!< Gyro Range +/-480 Degrees */
    GYRO_RANGE_960,     /*!< Gyro Range +/-960 Degrees */
    GYRO_RANGE_1920     /*!< Gyro Range +/-1920 Degrees */
};

/*! Table 47 and 51 Settings (Page 33 and 35) from MS-3020 Documentation.*/
enum turn_on_off {
    TURN_OFF,   /*!< Turn Off Code */
    TURN_ON     /*!< Turn On Code */
};

/*! Numerical Definition of Error Return Values.*/
enum return_values {
    ACK,                    /*!< No Error */
    ERR_MISC,               /*!< Error Miscellaneous */
    ERR_SYNC_BYTES,         /*!< Error with Sync Bytes */
    ERR_CHECKSUM_MISMATCH,  /*!< Error in Checksum */
    ERR_INVALID_MSG,        /*!< Error with Invalid Message */
    ERR_INVALID_PACKET,     /*!< Error with Invalid Packet */
    ERR_MSG_TYPE,           /*!< Error with Message Type */
    ERR_NAK,                /*!< Error with ACK Byte */
    ERR_MSG_ECHO,           /*!< Error with Message Echo */
    ERR_RTN                 /*!< Error with Return */
};

/*! Message Header Definition as Defined in MS-3020 Documentation.*/
struct __attribute__((__packed__)) message_header {
    uint16_t sync;              /*!< Sync Bytes */
    enum message_types type:8;  /*!< Message Type Code */
    uint8_t payload_size;       /*!< Payload Size in Bytes */
    enum message_codes code:8;  /*!< Message Code */
    uint8_t msg_len;            /*!< Message Length in Bytes */
};

/** @cond INCLUDE_WITH_DOXYGEN */
void set_leap_seconds(int l);
enum return_values unix_time_to_gps_time();
void make_imu_verbose();
uint16_t fletcher(uint8_t* msg, uint8_t len);
void set_baud_rate(unsigned int br);
void set_device_location(char *loc);
void set_num_samples(uint32_t s);
unsigned int get_baud_rate(bool v);
int construct_msg(struct message_header msgh, char *msg, unsigned int msgl);
enum return_values open_connection();
enum return_values validate_message(char *rcv, int br, struct message_header mh);
//Page 7
enum return_values ping();
//Page 8
enum return_values get_device_messages();
//Page 9
enum return_values reset();
//Pages 10 - 17
enum return_values get_device_param(enum message_codes c, char *beginStr);
//Page 10
enum return_values get_device_model();
//Page 12
enum return_values get_device_sn();
//Page 14
enum return_values get_device_fw();
//Page 16
enum return_values get_device_cal();
//Page 18
enum return_values set_gps_time(uint16_t gps_week, uint32_t time_seconds);
//Page 19
enum return_values set_uart_baud_rate(enum function_codes fn, enum uart_rates br);
//Page 22
enum return_values set_filter(enum function_codes fn, enum filter_settings fs);
//Page 23
enum return_values set_sample_rate(enum function_codes fn, uint16_t sr);
//Page 26
enum return_values select_sensors(enum function_codes fn, enum sensors *s);
//Page 27
enum return_values get_internal_sample_rate();
//Page 29
enum return_values set_accel_range(enum function_codes fn, enum accel_range ar);
//Page 31
enum return_values set_gyro_range(enum function_codes fn, enum gyro_range gr);
//Page 32
enum return_values set_all(enum function_codes fn);
//Page 33
enum return_values set_data_on_off(enum function_codes fn, enum turn_on_off of);
//Page 35
enum return_values set_xtrig_on_off(enum function_codes fn, enum turn_on_off of);
//Page 37
enum return_values select_sensors_b(enum function_codes fn, enum sensors *s);
//Page 39
enum return_values set_aux_accel_range(enum function_codes fn, enum accel_range ar);
//Page 41
void fetch_data();
//Validation Functions
enum filter_settings validate_filter_setting(uint32_t f, bool v);
enum gyro_range validate_gyro_range(uint32_t r, bool v);
enum accel_range validate_accel_range(uint32_t r, bool v);
enum uart_rates validate_uart_rates(uint32_t r, bool v);
enum function_codes str_to_function_code(char *s, bool v);
bool str_to_sensors(char **s, enum sensors *x, int max_len, bool v);
/** @endcond */
