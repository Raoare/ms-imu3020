/**
 * @file serial_conn.c
 * @author Richard Oare
 * @date 31 Jan 2019
 * This file defines the functionality to open, close, read, and write
 * data to and from a Memsense IMU-3020 via a serial connection.
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * ms-imu3020 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "serial_conn.h"

/** IMU file descriptor. */
static int fd = 0;
/** Level of verbose to be. */
static int std_verbose = 0;
/** IMU recieve buffer. */
char recvBuf[MAX_RECEIVED_PACKET_SIZE];

/** Sets flag to be verbose
 @return None
 */
void set_std_verbose() {
    std_verbose = 1;
}

/** Sets Linux interface atributes for the IMU
 @param[in] fd File descriptor for IMU
 @param[in] speed Baud rate for IMU
 @return 0 if successful, < 0 otherwise
 */
int set_interface_attribs (int fd, int speed) {
    struct termios tty;
    memset (&tty, 0, sizeof(tty));
    int temp_cflag = 0;

    if (tcgetattr (fd, &tty) != 0) {
        fprintf(stderr, "%s", "Cannot Get Attributes\n");
        return -1;
    }
    
    cfsetospeed (&tty, speed);
    cfsetispeed (&tty, speed);
    
    tty.c_iflag = 0;
    tty.c_iflag |= INPCK;
    tty.c_oflag = 0;
    tty.c_cflag = 0;
    tty.c_lflag = 0;
    tty.c_cflag |= (CLOCAL | CREAD | CS8 | speed);
    tty.c_cflag |= CMSPAR;  // Mark Parity
    
    tty.c_cc[VMIN]  = 0;    // read doesn't block
    tty.c_cc[VTIME] = 5;    // 0.5 seconds read timeout
    
    if (tcsetattr (fd, TCSANOW, &tty) != 0) {
        fprintf(stderr, "%s", "Cannot Set TTY Attributes\n");
        return -1;
    }
    
    return 0;
}

/** Initializes the IMU
 @param[in] file String for absolute path to IMU file location
 @param[in] br Baud rate to use for IMU
 @return 0 if successful, 1 if error opening IMU, < 0 if attributes
 cannot be set successfully
 */
int init(char *file, int br) {
    int rtn = 0;
    if (fd > 0) close(fd);
    if ((fd = open (file, O_RDWR | O_NOCTTY | O_SYNC | O_NDELAY)) < 0) return 1;
    if (((rtn = set_interface_attribs(fd,br)) < 0) || (lock_f(fd) < 0)) close(fd);
    return rtn;
}

/** Closes the IMU file descriptor if its open
 @return None
 */
void close_fd() {
    if (fd > 0) {
        ulock_f(fd);
        close(fd);
    }
}

/** Transmits and Receives a message to and from the IMU.  This
 function includes a timeout set at 1 second.
 @return Number of bytes read if successful, -1 if an error occured,
 and 0 if timeout is hit.
 */
int txrx (char *msg, int msg_len) {
    fd_set set;
    struct timeval timeout;
    int rv, i;
    
    if (std_verbose > 0) {
        printf("SENDING: ");
        for (i = 0; i < msg_len; i++) printf("%02x\t",msg[i]);
        printf("\n");
    }
    
    tcflush(fd, TCIOFLUSH);
    write(fd, msg, msg_len);
    
    FD_ZERO(&set); /* clear the set */
    FD_SET(fd, &set); /* add our file descriptor to the set */
    
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;
    
    rv = select(fd + 1, &set, NULL, NULL, &timeout);
    
    if (rv == -1 && std_verbose) fprintf(stderr, "%s", "error occured in txrx\n"); /* an error accured */
    else if (rv == 0 && std_verbose) fprintf(stderr, "%s", "timeout\n"); /* a timeout occured */
    else rv = read(fd, recvBuf, MAX_RECEIVED_PACKET_SIZE); /* there was data to read */
    
    if (std_verbose > 0) {
        printf("READ: ");
        for (i = 0; i < rv; i++) printf("%02x",recvBuf[i]);
        printf("\n");
    }
    
    return rv;
}

/** Reads from the IMU without transmitting anything.  This function
 includes a timeout set at 10 milliseconds.
 @return Number of bytes read if successful, -1 if an error occured,
 and 0 if the timeout is hit.
 */
int read_packet() {
    fd_set set;
    struct timeval timeout;
    int rv, i;
    
    FD_ZERO(&set);
    FD_SET(fd, &set);
    
    timeout.tv_sec = 0;
    timeout.tv_usec = 10000;
    
    //tcflush(fd, TCIOFLUSH);
    
    rv = select(fd + 1, &set, NULL, NULL, &timeout);
    
    if (rv == -1) fprintf(stderr, "%s", "error occured in read_packet\n"); /* an error accured */
    else if (rv == 0) fprintf(stderr, "%s", "read packet timeout\n"); /* a timeout occured */
    else rv = read(fd, recvBuf, MAX_RECEIVED_PACKET_SIZE); /* there was data to read */

    if (std_verbose > 0) {
        printf("READ: ");
        for (i = 0; i < rv; i++) printf("%02x",recvBuf[i]);
        printf("\n");
    }
    
    return rv;
}
