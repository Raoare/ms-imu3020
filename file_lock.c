/**
 * @file file_lock.c
 * @author Richard Oare
 * @date 31 Jan 2019
 * This file provides file locking functionality
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * ms-imu3020 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "file_lock.h"

/** Place write lock on file
 @param[in] fd File descriptor to place lock on
 @return 0 if successful, -1 otherwise
 */
int lock_f(int fd) {
    struct flock fl;
    
    fl.l_type   = F_WRLCK;  /* F_RDLCK, F_WRLCK, F_UNLCK    */
    fl.l_whence = SEEK_SET; /* SEEK_SET, SEEK_CUR, SEEK_END */
    fl.l_start  = 0;        /* Offset from l_whence         */
    fl.l_len    = 0;        /* length, 0 = to EOF           */
    fl.l_pid    = getpid(); /* our PID                      */
    
    return fcntl(fd, F_SETLK, &fl);  /* F_GETLK, F_SETLK, F_SETLKW */
}

/** Remove lock on file
 @param[in] fd File descriptor to place lock on
 @return 0 if successful, -1 otherwise
 */
int ulock_f(int fd) {
    struct flock fl;
    
    fl.l_type   = F_UNLCK;  /* F_RDLCK, F_WRLCK, F_UNLCK    */
    fl.l_whence = SEEK_SET; /* SEEK_SET, SEEK_CUR, SEEK_END */
    fl.l_start  = 0;        /* Offset from l_whence         */
    fl.l_len    = 0;        /* length, 0 = to EOF           */
    fl.l_pid    = getpid(); /* our PID                      */
    
    return fcntl(fd, F_SETLK, &fl);  /* F_GETLK, F_SETLK, F_SETLKW */
}
