/**
 * @file std_functions.h
 * @author Richard Oare
 * @date 15 Jan 2019
 * This file provides defnitions for std_function.c
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * ms-imu3020 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef STD_FUNCTIONS_H
#define STD_FUNCTIONS_H

#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <math.h>

/** @cond INCLUDE_WITH_DOXYGEN */
const int test_endianness;
/** @endcond */

/** Checks for bigendianness */
#define is_bigendian() ( (*(char*)&test_endianness) == 0 )

/** Maximum size of the line allowed to be input by the user */
#define MAX_LINE_SIZE 255
/** Number of arguments allowed to be input by the user */
#define MAX_NUM_ARGS 11
/** Whitespace used for separation */
#define WHITESPACE " \t\n"

/** SBAUD Command for User Input. */
#define CMD_SET_BAUD_RATE "sbaud"
/** HELP Command for User Input. */
#define CMD_HELP "help"
/** RESET Command for User Input. */
#define CMD_RESET "reset"
/** PING Command for User Input. */
#define CMD_PING "ping"
/** START Command for User Input. */
#define CMD_START "start"
/** STOP Command for User Input. */
#define CMD_STOP "stop"
/** EXIT Command for User Input. */
#define CMD_EXIT "exit"
/** QUIT Command for User Input. */
#define CMD_QUIT "quit"
/** SGPSTIME Command for User Input. */
#define CMD_SET_GPS_TIME "sgpstime"
/** PARAM Command for User Input. */
#define CMD_PARAM "param"
/** GMSG Command for User Input. */
#define CMD_GET_DEVICE_MESSAGES "gmsg"
/** SFILTER Command for User Input. */
#define CMD_SET_FILTER "sfilter"
/** SRATE Command for User Input. */
#define CMD_SET_SAMPLE_RATE "srate"
/** SSENSORS Command for User Input. */
#define CMD_SELECT_SENSORS "ssensors"
/** SACCEL Command for User Input. */
#define CMD_SET_ACCEL_RANGE "saccel"
/** SGYRO Command for User Input. */
#define CMD_SET_GYRO_RANGE "sgyro"
/** SALL Command for User Input. */
#define CMD_SET_ALL "sall"
/** STIME Command for User Input. */
#define CMD_SET_TIME "stime"
/** VERBOSE Command for User Input. */
#define CMD_VERBOSE "verbose"
/** DEVICE Command for User Input. */
#define CMD_DEVICE "device"
/** CMDLINE Command for User Input. */
#define CMD_CMDLINE "cmdline"

/** Program help for allowed arguments to main.c. */
#define USAGE_HELP "Valid arguments include:\n -b {baud rate} [Default: 921600]\n --device or -d {device location} [Default: /dev/serial0]\n -s {Number of Samples} [Default: Continuous]\n -t {timeout} [Default: Continuous]\n --cmdline [Default: No User Input Control]\n--help or -h This Help Page\n --verbose {all ('a')}\n --reset\n --ping\n --sgpstime {week} {seconds}\n --param\n --gmsg\n --sbaud {baud rate}\n --sfilter {function code} {filter (in Hertz)}\n --srate {function code} {sample rate (in Hertz)}\n --ssensors {sensor1} {sensor2} {sensor...} {sensorN}\n --saccel {function code} {accelerometer range}\n --sgyro {function code} {gyroscope range}\n --sall {function code}\n --stime\n For more information check the man pages\n"

/*! Used to Convert User Text to Value.*/
enum cmd_val {
    OPT_VERBOSE = (1 << 0),             /*!< Command Value for VERBOSE */
    OPT_CMD_LINE_CONTROL = (1 << 1),    /*!< Command Value for CMDLINE */
    OPT_SET_BAUD_RATE = (1 << 2),       /*!< Command Value for SBAUD */
    OPT_HELP = (1 << 3),                /*!< Command Value for HELP */
    OPT_RESET = (1 << 4),               /*!< Command Value for RESET */
    OPT_PING = (1 << 5),                /*!< Command Value for PING */
    OPT_START = (1 << 6),               /*!< Command Value for START */
    OPT_STOP = (1 << 7),                /*!< Command Value for STOP */
    OPT_EXIT = (1 << 8),                /*!< Command Value for EXIT/QUIT */
    OPT_SET_GPS_TIME = (1 << 9),        /*!< Command Value for SGPSTIME */
    OPT_GET_PARAMETERS = (1 << 10),     /*!< Command Value for PARAM */
    OPT_GET_DEVICE_MESSAGES = (1 << 11),/*!< Command Value for GMSG */
    OPT_SET_FILTER = (1 << 12),         /*!< Command Value for SFILTER */
    OPT_SET_SAMPLE_RATE = (1 << 13),    /*!< Command Value for SRATE */
    OPT_SELECT_SENSORS = (1 << 14),     /*!< Command Value for SSENSORS */
    OPT_SET_ACCEL_RANGE = (1 << 15),    /*!< Command Value for SACCEL */
    OPT_SET_GYRO_RANGE = (1 << 16),     /*!< Command Value for SGYRO */
    OPT_SET_ALL = (1 << 17),            /*!< Command Value for SALL */
    OPT_SET_TIME = (1 << 18)            /*!< Command Value for STIME */
};

/** @cond INCLUDE_WITH_DOXYGEN */
int my_strcmp(char *s1, char *s2);
enum cmd_val decode_cmd(char *line, char **args, int maxNumArgs);
bool correct_num_args(char **args, int minNumArgs, int maxNumArgs, bool v);
void* array_to_val(uint8_t *a, int size);
/** @endcond */

#endif
