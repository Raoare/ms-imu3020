/**
 * @file signal_handler.h
 * @author Richard Oare
 * @date 15 Jan 2019
 * This file provides definitions for signal_handler.c
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * ms-imu3020 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef sig_handler_h
#define sig_handler_h

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

/** @cond INCLUDE_WITH_DOXYGEN */
void handle_error_int(int sig);
void handle_term_int(int sig);
void init_error_sig(void (*handler) (void));
void init_term_sig(void (*handler) (void));
void init_sig(void (*error_handler) (void), void (*term_handler) (void));
/** @endcond */

#endif
