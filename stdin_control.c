/**
 * @file stdin_control.c
 * @author Richard Oare
 * @date 15 Jan 2019
 * This file provides control for in-program user commands
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * ms-imu3020 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "stdin_control.h"

/** Argument array to place user arguments. */
uint32_t stdin_arg[MAX_NUM_ARGS];

/** Fetches and decodes user input during execution
 @param[in] v T/F to be verbose or not
 @return cmd_val
 */
enum cmd_val fetch_decode(bool v) {
    char line[MAX_LINE_SIZE];
    char *args[MAX_NUM_ARGS];
    enum cmd_val cmd = 0;
    
    if ((NULL != fgets(line, 255, stdin))) cmd = decode_cmd(line, args, MAX_NUM_ARGS);
        
    switch (cmd) {
        case OPT_SET_BAUD_RATE:
            if (!correct_num_args(args,2,2,v)) cmd = 0;
            else {
                stdin_arg[0] = str_to_function_code(args[0], v);
                stdin_arg[1] = validate_uart_rates(strtol(args[1], NULL, 10), v);
                if (stdin_arg[0] == 0 || stdin_arg[1] == 0) cmd = 0;
            }
            break;
        case OPT_HELP:
            if (!correct_num_args(args,0,0,v)) cmd = 0;
            else printf("%s",USAGE_HELP);
            break;
        case OPT_RESET:
            if (!correct_num_args(args,0,0,v)) cmd = 0;
            break;
        case OPT_PING:
            if (!correct_num_args(args,0,0,v)) cmd = 0;
            break;
        case OPT_START:
            if (!correct_num_args(args,0,0,v)) cmd = 0;
            break;
        case OPT_STOP:
            if (!correct_num_args(args,0,0,v)) cmd = 0;
            break;
        case OPT_EXIT:
            if (!correct_num_args(args,0,0,v)) cmd = 0;
            break;
        case OPT_SET_GPS_TIME:
            if (!correct_num_args(args,2,2,v)) cmd = 0;
            else {
                stdin_arg[0] = strtol(args[0], NULL, 10);
                stdin_arg[1] = strtol(args[1], NULL, 10);
                if (stdin_arg[0] == 0 || stdin_arg[1] == 0) cmd = 0;
            }
            break;
        case OPT_GET_PARAMETERS:
            if (!correct_num_args(args,0,0,v)) cmd = 0;
            break;
        case OPT_GET_DEVICE_MESSAGES:
            if (!correct_num_args(args,0,0,v)) cmd = 0;
            break;
        case OPT_SET_FILTER:
            if (!correct_num_args(args,2,2,v)) cmd = 0;
            else {
                stdin_arg[0] = str_to_function_code(args[0], v);
                stdin_arg[1] = validate_filter_setting(strtol(args[1],NULL,10),v);
                if (stdin_arg[0] == 0 || stdin_arg[1] == 0) {
                    cmd = 0;
                    if (v) fprintf(stderr, "Invalid Filter Setting: %d\n", stdin_arg[1]);
                }
            }
            break;
        case OPT_SET_SAMPLE_RATE:
            if (!correct_num_args(args,2,2,v)) cmd = 0;
            else {
                stdin_arg[0] = str_to_function_code(args[0], v);
                stdin_arg[1] = strtol(args[1], NULL, 10);
                printf("srate: %d\n",stdin_arg[1]);
                if (stdin_arg[0] == 0 || stdin_arg[1] > MAX_SAMPLE_RATE || stdin_arg[1] < MIN_SAMPLE_RATE) {
                    cmd = 0;
                    if (v) fprintf(stderr, "Invalid Sample Rate: %d\n", stdin_arg[1]);
                }
            }
            break;
        case OPT_SELECT_SENSORS:
            if (!correct_num_args(args,2,10,v)) cmd = 0;
            else if (0 == (stdin_arg[0] = str_to_function_code(args[0], v))) cmd = 0;
            else if (!str_to_sensors(&(args[1]), &(stdin_arg[1]), MAX_NUM_ARGS, v)) cmd = 0;
            break;
        case OPT_SET_ACCEL_RANGE:
            if (!correct_num_args(args,2,2,v)) cmd = 0;
            else {
                stdin_arg[0] = str_to_function_code(args[0], v);
                stdin_arg[1] = validate_accel_range(strtol(args[1],NULL,10),v);
                if (stdin_arg[0] == 0 || stdin_arg[1] == 0) cmd = 0;
            }
            break;
        case OPT_SET_GYRO_RANGE:
            if (!correct_num_args(args,2,2,v)) cmd = 0;
            else {
                stdin_arg[0] = str_to_function_code(args[0], v);
                stdin_arg[1] = validate_gyro_range(strtol(args[1], NULL, 10),v);
                if (stdin_arg[0] == 0 || stdin_arg[1] == 0) cmd = 0;
            }
            break;
        case OPT_SET_ALL:
            if (!correct_num_args(args,1,1,v)) cmd = 0;
            else if (0 == (stdin_arg[0] = str_to_function_code(args[0],v))) cmd = 0;
            break;
        case OPT_SET_TIME:
            if (!correct_num_args(args,0,0,v)) cmd = 0;
            break;
        default:
            if (v) fprintf(stderr, "%s", "Invalid Command\n");
            break;
    }
    
    return cmd;
}
