/**
 * @file imu_interrupt.c
 * @author Richard Oare
 * @date 15 Jan 2019
 * This file provides functionality for a pin interrupt for the TOV
 * line on the MS-3020.  This depends on the wiringPi library.
 * @copyright GNU Public License
 * @page License
 * Copyright (C) 2019 Richard Oare
 * All Rights Reserved.
 *
 * ms-imu3020 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "imu_interrupt.h"

/** Initializes pin interrupt using wiringPi
 @param[in] imu_interrupt_handler Handler to call upon interrupt
 @return None
 */
void imu_interrupt_init(void (*imu_interrupt_handler)(void)) {
    wiringPiSetup();
    pinMode(TOV, INPUT);
    pinMode(PPS, OUTPUT);
    pinMode(XTRIG, OUTPUT);
    digitalWrite(PPS, HIGH);
    digitalWrite(XTRIG, HIGH);
    wiringPiISR(TOV, INT_EDGE_RISING, imu_interrupt_handler);
}

/** Stops interrupt
 @return None
 */
void stop_interrupt() {
    system("$(which gpio) edge 25 none");
}

/** Restarts interrupt previously stopped.  Must be initialized.
 @return None
 */
void restart_interrupt() {
    system("$(which gpio) edge 25 falling");
}
